package org.adfemg.datacontrol.xml.ha;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
                    OperationStateTest.class, SerializableDocumentTest.class, SerializableParametersTest.class,
                    SerializableUserDataTest.class })
public class HaSuite {
}
