package org.adfemg.datacontrol.xml.utils;

import java.util.regex.Pattern;

import org.junit.Assert;
import org.junit.Test;

import org.w3c.dom.Element;

import org.xml.sax.SAXException;

public class XmlParseUtilsTest {

    @Test
    public void compactNodeToIndentedString() throws SAXException {
        final String srcxml = "<element><child/></element>";
        Element element = XmlParseUtils.parse(srcxml).getDocumentElement();
        String str = XmlParseUtils.nodeToIndentedString(element);
        Assert.assertTrue("not properly indented", Pattern.matches("<element>\r?\n +<child/>\r?\n</element>", str));
    }

    @Test
    public void whitespaceNodeToIndentedString() throws SAXException {
        // default XSLT will not indent element that already contain whitespace (such as <element> in this example)
        final String srcxml = "<element> <child/></element>";
        Element element = XmlParseUtils.parse(srcxml).getDocumentElement();
        String str = XmlParseUtils.nodeToIndentedString(element);
        Assert.assertTrue("not properly indented", Pattern.matches("<element>\r?\n +<child/>\r?\n</element>", str));
    }

    @Test
    public void newlineNodeToIndentedString() throws SAXException {
        // default XSLT will not indent element that already contain whitespace (such as <element> in this example)
        final String srcxml = "<element>\n <child/>\n</element>";
        Element element = XmlParseUtils.parse(srcxml).getDocumentElement();
        String str = XmlParseUtils.nodeToIndentedString(element);
        // test that at least additiona whitespace has been added before child (minimal 2 spaces)
        Assert.assertTrue("not properly indented", Pattern.matches("<element>\r?\n {2,}<child/>\r?\n</element>", str));
    }

    @Test
    public void namespaceNodeToIndentedString() throws SAXException {
        final String srcxml = "<x:element   xmlns:x='xmlns.example.com'>  <child/></x:element>";
        Element element = XmlParseUtils.parse(srcxml).getDocumentElement();
        String str = XmlParseUtils.nodeToIndentedString(element);
        Assert.assertTrue("not properly indented",
                          Pattern.matches("<x:element xmlns:x=\"xmlns.example.com\">\r?\n +<child/>\r?\n</x:element>",
                                          str));
    }

}
