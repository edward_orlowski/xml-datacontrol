package org.adfemg.datacontrol.xml;

import java.io.Reader;
import java.io.StringReader;

import java.net.URL;

import java.util.List;

import oracle.binding.meta.StructureDefinition;

import oracle.xml.parser.schema.XMLSchema;
import oracle.xml.parser.schema.XSDBuilder;
import oracle.xml.parser.schema.XSDElement;
import oracle.xml.parser.schema.XSDException;

import org.adfemg.datacontrol.xml.data.XMLDCElement;
import org.adfemg.datacontrol.xml.provider.Provider;
import org.adfemg.datacontrol.xml.provider.structure.MovableStructureDefinition;
import org.adfemg.datacontrol.xml.provider.structure.SchemaStructureProvider;
import org.adfemg.datacontrol.xml.provider.typemap.TypeMapper;
import org.adfemg.datacontrol.xml.provider.typemap.TypeMapperImpl;
import org.adfemg.datacontrol.xml.utils.DomUtils;
import org.adfemg.datacontrol.xml.utils.XmlFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


/**
 * Helper class to create a department XMLDCElement which can be used in the JUnit tests.
 */
public class XMLBuilderHelper {

    private SchemaStructureProvider structureProvider = new SchemaStructureProvider();
    private TypeMapper typeMapper = new TypeMapperImpl();
    private Document document = XmlFactory.newDocument();

    public static final String NAMESPACE_URI = "http://xmlns.example.com";
    public static final String SCHEMA_ROOT_ELEM =
        "<xsd:schema xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:tns='http://xmlns.example.com' targetNamespace='http://xmlns.example.com' elementFormDefault='qualified'>";

    /**
     * Creates a new Department XMLDCElement.
     * @param elements List of Element types which are added to the Department XMLDCElement, can be {@code null}.
     * @param changeTracking Boolean to set change-tracking on or off.
     * @return A new XMLDCElement.
     * @throws XSDException Thrown when the XSDSchema could not be parsed.
     */
    public XMLDCElement buildDepartmentXMLDCElement(final List<Element> elements,
                                                    final boolean changeTracking) throws XSDException {
        XMLSchema schema = buildDepartmentXMLSchema();
        XSDElement xsdElem = schema.getElement(schema.getSchemaTargetNS(), "DepartmentList");
        String structName = "dc.methodReturn";

        MovableStructureDefinition struct = structureProvider.buildStructure(xsdElem, structName, typeMapper);
        Element deptListElement = document.createElementNS(schema.getSchemaTargetNS(), xsdElem.getName());
        addElements(deptListElement, elements);

        return new XMLDCElement(getDataControl(changeTracking), struct, deptListElement);
    }

    /**
     * Creates a new Department Element.
     *
     * @param name The value of the Child element 'name'.
     * @param location The value of the attribute 'location'.
     * @return The Department Element.
     */
    public Element createDepartment(String name, String location) {
        Element department = document.createElementNS(NAMESPACE_URI, "Department");
        if (name != null) {
            DomUtils.addChildElement(department, NAMESPACE_URI, "name", name);
        }
        department.setAttribute("location", location);
        return department;
    }

    private XMLSchema buildDepartmentXMLSchema() throws XSDException {
        // this test is probably too long and misses some assertions, but it does show the process from parsing XSD
        // to building a XML Document through XMLDCElement and XMLDCCollection
        String xsd = SCHEMA_ROOT_ELEM;
        xsd += "  <xsd:element name='DepartmentList'>";
        xsd += "    <xsd:complexType>";
        xsd += "      <xsd:sequence>";
        xsd += "        <xsd:element name='Department' maxOccurs='unbounded'>";
        xsd += "          <xsd:complexType>";
        xsd += "            <xsd:sequence>";
        xsd += "              <xsd:element name='name' type='xsd:string'/>";
        xsd += "            </xsd:sequence>";
        xsd += "            <xsd:attribute name='location' type='xsd:string'/>";
        xsd += "          </xsd:complexType>";
        xsd += "        </xsd:element>";
        xsd += "      </xsd:sequence>";
        xsd += "    </xsd:complexType>";
        xsd += "  </xsd:element>";
        xsd += "</xsd:schema>";
        return parseSchemaDocument(xsd);
    }

    private static XMLSchema parseSchemaDocument(String xsd) throws XSDException {
        Reader reader = new StringReader(xsd);
        URL baseUrl = null;
        XSDBuilder builder = new XSDBuilder();
        return builder.build(reader, baseUrl);
    }

    private static DataControl getDataControl(final boolean changeTracking) {
        // DataControl is needed by XMLDCElement.getTypeMapper()
        // for DataControl.getDCDefinition().findDefinitionNode(StructureDefinition).getProviderInstance(TypeMapper.class)
        // TODO: that's an ugly dependency
        return new DataControl() {
            @Override
            public DataControlDefinition getDCDefinition() {
                return new DataControlDefinition() {
                    @Override
                    public DataControlDefinitionNode findDefinitionNode(StructureDefinition structDef) {
                        return new DataControlDefinitionNode() {
                            @Override
                            public <T extends Provider> T getProviderInstance(Class<T> iface) {
                                return (T) new TypeMapperImpl();
                            }

                            @Override
                            public boolean isChangeTracking() {
                                return changeTracking;
                            }
                        };
                    }
                };
            }
        };
    }

    private void addElements(Element parent, List<Element> elements) {
        if (elements != null) {
            for (Element child : elements) {
                parent.appendChild(child);
            }
        }
    }

    public SchemaStructureProvider getStructureProvider() {
        return structureProvider;
    }

    public TypeMapper getTypeMapper() {
        return typeMapper;
    }

}
