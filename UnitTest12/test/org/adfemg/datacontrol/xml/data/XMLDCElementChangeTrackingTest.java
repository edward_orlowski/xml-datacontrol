package org.adfemg.datacontrol.xml.data;

import java.util.ArrayList;
import java.util.List;

import org.adfemg.datacontrol.xml.XMLBuilderHelper;
import org.adfemg.datacontrol.xml.utils.XmlParseUtils;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import org.w3c.dom.Element;

public class XMLDCElementChangeTrackingTest {

    private XMLDCElement xmldcDepartmentList;
    private XMLDCCollection departments;
    private XMLDCElement departmentFinance;
    private XMLDCElement departmentSpecial;
    private XMLDCElement departmentNoName;
    private XMLDCElement departmentEmptyName;

    @Before
    public void setUp() throws Exception {
        final XMLBuilderHelper xmlBuilder = new XMLBuilderHelper();

        // create list of department elements
        final List<Element> elements = new ArrayList<>();
        elements.add(xmlBuilder.createDepartment("finance", "The Hague"));
        elements.add(xmlBuilder.createDepartment("special '\"<?!ö>\"'", "Att special '\"<?!ö>\"'"));
        elements.add(xmlBuilder.createDepartment(null, "Amsterdam")); // without name
        elements.add(xmlBuilder.createDepartment("", "Rotterdam")); // with empty name

        // create new DepartmentList Root Element with changtracking on and initial departments
        this.xmldcDepartmentList = xmlBuilder.buildDepartmentXMLDCElement(elements, true);
        this.departments = (XMLDCCollection) xmldcDepartmentList.get("Department");

        this.departmentFinance = departments.get(0);
        this.departmentSpecial = departments.get(1);
        this.departmentNoName = departments.get(2);
        this.departmentEmptyName = departments.get(3);
    }

    @Test
    public void testGetAttribute() {
        assertEquals("getting string element name", "finance", departments.get(0).get("name"));
        assertEquals("getting string attribute location", "The Hague", departments.get(0).get("location"));
    }

    @Test
    public void testCreateElement() {
        // create new department XMLDCElement with name 'sales'
        XMLDCElement departmentSales = departments.createElement(0, xmldcDepartmentList);
        departmentSales.put("name", "sales");
        departmentSales.put("location", "Utrecht");

        // assert that the department Element is created with name 'sales' and attribute location must be 'Utrecht'
        String xmlString = XmlParseUtils.nodeToString(departmentSales.getElement());
        String expectedString =
            "<Department location=\"Utrecht\" xmlns=\"http://xmlns.example.com\"><?change-attribute-value old=\"\" name=\"location\"?><?change-inserted ?><name><?change-value old=\"\"?>sales</name></Department>";
        assertEquals("created department is not correct, name must be 'sales' and attribute 'location' must be 'Utrecht",
                     expectedString, xmlString);
        assertEquals("department name must be 'sales'", "sales", departmentSales.get("name"));
        assertEquals("department attribute location must be 'Utrecht'", "Utrecht", departmentSales.get("location"));
        assertTrue("name changed", departmentSales.isChanged("name"));
        assertTrue("location changed", departmentSales.isChanged("location"));
        assertEquals(null, departmentSales.getOldValue("name"));
        assertEquals(null, departmentSales.getOldValue("location"));
    }

    @Test
    public void testChangeElement() {
        // change the finance department Element name to 'operations'
        assertFalse("name changed", departmentFinance.isChanged("name"));
        departmentFinance.put("name", "operations");
        String xmlString = XmlParseUtils.nodeToString(departmentFinance.getElement());
        String expectedString =
            "<Department location=\"The Hague\" xmlns=\"http://xmlns.example.com\"><name><?change-value old=\"finance\"?>operations</name></Department>";
        assertEquals("department not correct, name must be 'operations'", expectedString, xmlString);
        assertEquals("department name must be 'operations'", "operations", departmentFinance.get("name"));
        assertTrue("name changed", departmentFinance.isChanged("name"));
        assertEquals("finance", departmentFinance.getOldValue("name"));
    }

    @Test
    public void testChangeSameElement() {
        // change the finance department Element name to 'operations'
        assertFalse("name changed", departmentFinance.isChanged("name"));
        departmentFinance.put("name", "finance");
        String xmlString = XmlParseUtils.nodeToString(departmentFinance.getElement());
        String expectedString =
            "<Department location=\"The Hague\" xmlns=\"http://xmlns.example.com\"><name><?change-value old=\"finance\"?>finance</name></Department>";
        assertEquals("department not correct, name must be 'finance'", expectedString, xmlString);
        assertEquals("department name must be 'finance'", "finance", departmentFinance.get("name"));
        assertTrue("name changed", departmentFinance.isChanged("name"));
        assertEquals("finance", departmentFinance.getOldValue("name"));
    }

    @Test
    public void testChangeNewSimpleElement() {
        String xmlString = XmlParseUtils.nodeToString(departmentNoName.getElement());
        assertFalse("name changed", departmentNoName.isChanged("name"));
        departmentNoName.put("name", "operations");
        xmlString = XmlParseUtils.nodeToString(departmentNoName.getElement());
        String expectedString =
            "<Department location=\"Amsterdam\" xmlns=\"http://xmlns.example.com\"><name><?change-value old=\"\"?>operations</name></Department>";
        assertEquals("department not correct, name must be 'operations'", expectedString, xmlString);
        assertEquals("department name must be 'operations'", "operations", departmentNoName.get("name"));
        assertTrue("name changed", departmentNoName.isChanged("name"));
        assertEquals(null, departmentNoName.getOldValue("name"));
    }

    @Test
    public void testMultiChangeNewSimpleElement() {
        assertFalse("name changed", departmentNoName.isChanged("name"));
        departmentNoName.put("name", "operations");
        assertTrue("name changed", departmentNoName.isChanged("name"));
        departmentNoName.put("name", "sales"); // should not override processing instruction
        String xmlString = XmlParseUtils.nodeToString(departmentNoName.getElement());
        String expectedString =
            "<Department location=\"Amsterdam\" xmlns=\"http://xmlns.example.com\"><name><?change-value old=\"\"?>sales</name></Department>";
        assertEquals("department not correct, name must be 'sales'", expectedString, xmlString);
        assertEquals("department name must be 'sales'", "sales", departmentNoName.get("name"));
        assertTrue("name changed", departmentNoName.isChanged("name"));
        assertEquals(null, departmentNoName.getOldValue("name"));
    }

    @Test
    public void testChangeEmptySimpleElement() {
        String xmlString = XmlParseUtils.nodeToString(departmentEmptyName.getElement());
        assertFalse("name changed", departmentEmptyName.isChanged("name"));
        departmentEmptyName.put("name", "operations");
        xmlString = XmlParseUtils.nodeToString(departmentEmptyName.getElement());
        String expectedString =
            "<Department location=\"Rotterdam\" xmlns=\"http://xmlns.example.com\"><name><?change-value old=\"\"?>operations</name></Department>";
        assertEquals("department not correct, name must be 'operations'", expectedString, xmlString);
        assertEquals("department name must be 'operations'", "operations", departmentEmptyName.get("name"));
        assertTrue("name changed", departmentEmptyName.isChanged("name"));
        assertEquals(null, departmentEmptyName.getOldValue("name"));
    }

    @Test
    public void testMultiChangeEmptySimpleElement() {
        assertFalse("name changed", departmentEmptyName.isChanged("name"));
        departmentEmptyName.put("name", "operations");
        assertTrue("name changed", departmentEmptyName.isChanged("name"));
        departmentEmptyName.put("name", "sales"); // should not override processing instruction
        String xmlString = XmlParseUtils.nodeToString(departmentEmptyName.getElement());
        String expectedString =
            "<Department location=\"Rotterdam\" xmlns=\"http://xmlns.example.com\"><name><?change-value old=\"\"?>sales</name></Department>";
        assertEquals("department not correct, name must be 'sales'", expectedString, xmlString);
        assertEquals("department name must be 'sales'", "sales", departmentEmptyName.get("name"));
        assertTrue("name changed", departmentEmptyName.isChanged("name"));
        assertEquals(null, departmentEmptyName.getOldValue("name"));
    }

    @Test
    public void testClearElement() {
        // insert "" for the Element name and assert that Processing Instruction 'change-value' is added
        assertFalse("name changed", departmentFinance.isChanged("name"));
        departmentFinance.put("name", "");
        String xmlString = XmlParseUtils.nodeToString(departmentFinance.getElement());
        String expectedString =
            "<Department location=\"The Hague\" xmlns=\"http://xmlns.example.com\"><name><?change-value old=\"finance\"?></name></Department>";
        assertEquals("department not correct, name must be empty", expectedString, xmlString);
        assertNull("department name must be removed", departmentFinance.get("name"));
        assertTrue("name changed", departmentFinance.isChanged("name"));
        assertEquals("finance", departmentFinance.getOldValue("name"));
    }

    @Test
    public void testRemoveElement() {
        // insert null for the Element name and assert that Processing Instruction 'change-value' is added
        assertFalse("name changed", departmentFinance.isChanged("name"));
        departmentFinance.put("name", null);
        String xmlString = XmlParseUtils.nodeToString(departmentFinance.getElement());
        String expectedString =
            "<Department location=\"The Hague\" xmlns=\"http://xmlns.example.com\"><name><?change-value old=\"finance\"?></name></Department>";
        assertEquals("department not correct, name must be empty", expectedString, xmlString);
        assertNull("department name must be removed", departmentFinance.get("name"));
        assertTrue("name changed", departmentFinance.isChanged("name"));
        assertEquals("finance", departmentFinance.getOldValue("name"));
    }

    @Test
    public void testMultipleChangesElement() {
        // change the finance department Element name to 'sales' and then to 'marketing'
        assertFalse("name changed", departmentFinance.isChanged("name"));
        departmentFinance.put("name", "sales");
        assertTrue("name changed", departmentFinance.isChanged("name"));
        String xmlString = XmlParseUtils.nodeToString(departmentFinance.getElement());
        departmentFinance.put("name", "marketing");
        xmlString = XmlParseUtils.nodeToString(departmentFinance.getElement());
        String expectedString =
            "<Department location=\"The Hague\" xmlns=\"http://xmlns.example.com\"><name><?change-value old=\"finance\"?>marketing</name></Department>";
        assertEquals("department not correct, name must be 'marketing' and change-value old must be 'finance'",
                     expectedString, xmlString);
        assertEquals("department name must be 'marketing'", "marketing", departmentFinance.get("name"));
        assertTrue("name changed", departmentFinance.isChanged("name"));
        assertEquals("finance", departmentFinance.getOldValue("name"));
    }

    @Test
    public void testChangeAttribute() {
        // change the attribute location to 'Amsterdam'
        assertFalse("location changed", departmentFinance.isChanged("location"));
        departmentFinance.put("location", "Amsterdam");
        String xmlString = XmlParseUtils.nodeToString(departmentFinance.getElement());
        String expectedString =
            "<Department location=\"Amsterdam\" xmlns=\"http://xmlns.example.com\"><?change-attribute-value old=\"The Hague\" name=\"location\"?><name>finance</name></Department>";
        assertEquals("department not correct, attribute location must be 'Amsterdam'", expectedString, xmlString);
        assertEquals("department location must be 'Amsterdam'", "Amsterdam", departmentFinance.get("location"));
        assertTrue("location changed", departmentFinance.isChanged("location"));
        assertEquals("The Hague", departmentFinance.getOldValue("location"));
    }

    @Test
    public void testChangeSameAttribute() {
        // change the attribute location to 'Amsterdam'
        assertFalse("location changed", departmentFinance.isChanged("location"));
        departmentFinance.put("location", "The Hague");
        String xmlString = XmlParseUtils.nodeToString(departmentFinance.getElement());
        String expectedString =
            "<Department location=\"The Hague\" xmlns=\"http://xmlns.example.com\"><?change-attribute-value old=\"The Hague\" name=\"location\"?><name>finance</name></Department>";
        assertEquals("department not correct, attribute location must be 'The Hague'", expectedString, xmlString);
        assertEquals("department location must be 'The Hague'", "The Hague", departmentFinance.get("location"));
        assertTrue("location changed", departmentFinance.isChanged("location"));
        assertEquals("The Hague", departmentFinance.getOldValue("location"));
    }

    @Test
    public void testClearAttribute() {
        // insert "" for the Attribute location and assert that the Attribute is empty but not removed
        assertFalse("location changed", departmentFinance.isChanged("location"));
        departmentFinance.put("location", "");
        String xmlString = XmlParseUtils.nodeToString(departmentFinance.getElement());
        String expectedString =
            "<Department location=\"\" xmlns=\"http://xmlns.example.com\"><?change-attribute-value old=\"The Hague\" name=\"location\"?><name>finance</name></Department>";
        assertEquals("department not correct, name must be sales and attribute location must be empty", expectedString,
                     xmlString);
        assertNull("department location should be removed", departmentFinance.get("location"));
        assertTrue("location changed", departmentFinance.isChanged("location"));
        assertEquals("The Hague", departmentFinance.getOldValue("location"));
    }

    @Test
    public void testRemoveAttribute() {
        // insert null for the Attribute location and assert that the Attribute is empty but not removed
        assertFalse("location changed", departmentFinance.isChanged("location"));
        departmentFinance.put("location", null);
        String xmlString = XmlParseUtils.nodeToString(departmentFinance.getElement());
        String expectedString =
            "<Department location=\"\" xmlns=\"http://xmlns.example.com\"><?change-attribute-value old=\"The Hague\" name=\"location\"?><name>finance</name></Department>";
        assertEquals("department not correct, name must be sales and attribute location must be empty", expectedString,
                     xmlString);
        assertNull("department location should be removed", departmentFinance.get("location"));
        assertTrue("location changed", departmentFinance.isChanged("location"));
        assertEquals("The Hague", departmentFinance.getOldValue("location"));
    }

    @Test
    public void testMultipleChangesAttribute() {
        // change the finance department Atttribute location to 'Utrecht' and then to 'Amsterdam'
        assertFalse("location changed", departmentFinance.isChanged("location"));
        departmentFinance.put("location", "Utrecht");
        assertTrue("location changed", departmentFinance.isChanged("location"));
        departmentFinance.put("location", "Amsterdam");
        String xmlString = XmlParseUtils.nodeToString(departmentFinance.getElement());
        String expectedString =
            "<Department location=\"Amsterdam\" xmlns=\"http://xmlns.example.com\"><?change-attribute-value old=\"The Hague\" name=\"location\"?><name>finance</name></Department>";
        assertEquals("department not correct, location must be 'Amsterdam' and change-attribute-value old must be 'The Hague'",
                     expectedString, xmlString);
        assertEquals("department location must be 'Amsterdam'", "Amsterdam", departmentFinance.get("location"));
        assertTrue("location changed", departmentFinance.isChanged("location"));
        assertEquals("The Hague", departmentFinance.getOldValue("location"));
    }

    @Test
    public void testSpecialCharacters() {
        // test with special character string: '"<?!>"'
        final String specialString = "'\"<?!öí>\"'";

        departmentSpecial.put("name", specialString);
        departmentSpecial.put("location", specialString);
        String xmlString = XmlParseUtils.nodeToString(departmentSpecial.getElement());
        String expectedString =
            "<Department location=\"'&quot;&lt;?!öí>&quot;'\" xmlns=\"http://xmlns.example.com\"><?change-attribute-value old=\"Att special '\"<?!ö>\"'\" name=\"location\"?><name><?change-value old=\"special '\"<?!ö>\"'\"?>'\"&lt;?!öí>\"'</name></Department>";
        assertEquals("department not correct, location must be 'specialString' and change-attribute-value old must be 'specialString'",
                     expectedString, xmlString);
        assertEquals("special '\"<?!ö>\"'", departmentSpecial.getOldValue("name"));
        assertEquals("Att special '\"<?!ö>\"'", departmentSpecial.getOldValue("location"));
    }

}
