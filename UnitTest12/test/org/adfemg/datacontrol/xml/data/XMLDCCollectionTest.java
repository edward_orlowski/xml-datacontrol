package org.adfemg.datacontrol.xml.data;

import java.util.Collections;

import oracle.xml.parser.schema.XSDException;

import org.adfemg.datacontrol.xml.XMLBuilderHelper;
import org.adfemg.datacontrol.xml.utils.ChangeTrackingUtils;
import org.adfemg.datacontrol.xml.utils.XmlParseUtils;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import org.w3c.dom.Element;


public class XMLDCCollectionTest {

    private XMLDCElement xmldcDepartmentList;
    private XMLDCCollection departments;
    private XMLDCElement departmentSales;

    @Before
    public void setUp() throws Exception {
        // create new (empty) DepartmentList Root Element with changtracking off
        XMLBuilderHelper xmlBuilder = new XMLBuilderHelper();
        xmldcDepartmentList = xmlBuilder.buildDepartmentXMLDCElement(null, false);

        // create new department XMLDCElement with name 'sales'
        departments = (XMLDCCollection) xmldcDepartmentList.get("Department");
        departmentSales = departments.createElement(-1, xmldcDepartmentList);
        departmentSales.put("name", "sales");
    }

    @Test
    public void testInitialCollection() {
        assertFalse(xmldcDepartmentList.isChangeTracking());
        // assert that the departmentlist is created with one sales department in setUp
        String xmlString = XmlParseUtils.nodeToString(xmldcDepartmentList.getElement());
        String expectedString =
            "<DepartmentList xmlns=\"http://xmlns.example.com\"><Department><name>sales</name></Department></DepartmentList>";
        assertEquals("created departmentList is not correct, expecting one department with name 'sales'",
                     expectedString, xmlString);
        assertSame("Department 'sales' references are not the same", departmentSales, departments.get(0));
    }

    @Test
    public void testCreateElement() {
        // create a new marketing department and add it to the departmentlist as the first element
        XMLDCElement departmentMarketing = departments.createElement(0, xmldcDepartmentList);
        departmentMarketing.put("name", "marketing");

        // assert that the departmentlist has two departments, marketing and sales
        String xmlString = XmlParseUtils.nodeToString(xmldcDepartmentList.getElement());
        String expectedString =
            "<DepartmentList xmlns=\"http://xmlns.example.com\"><Department><name>marketing</name></Department><Department><name>sales</name></Department></DepartmentList>";
        assertEquals("created departmentList is not correct, two departments with name 'sales' and 'marketing' are expected",
                     expectedString, xmlString);
        assertSame("Deparment 'sales' references are not the same", departmentSales, departments.get(1));
        assertSame("Deparment 'marketing' references are not the same", departmentMarketing, departments.get(0));
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void testCreateElementTooLargeIndex() {
        assertEquals("initial list should contain single element", 1, departments.size());
        departments.createElement(1, xmldcDepartmentList);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void testCreateElementMinusOneIndexNonEmptyCollection() {
        assertEquals("initial list should contain single element", 1, departments.size());
        departments.createElement(-1, xmldcDepartmentList);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void testCreateElementZeroIndexEmptyCollection() {
        assertEquals("initial list should contain single element", 1, departments.size());
        departments.remove(0);
        assertEquals(0, departments.size());
        departments.createElement(0, xmldcDepartmentList);
    }

    @Test
    public void testRemove() {
        // remove the sales department
        departments.remove(0);

        // assert that the sales department is removed from the list
        String xmlString = XmlParseUtils.nodeToString(xmldcDepartmentList.getElement());
        //System.out.println(xmlString);
        String expectedString = "<DepartmentList xmlns=\"http://xmlns.example.com\"/>";
        assertEquals("departmentList is not correct, sales department should be removed from the tree", expectedString,
                     xmlString);
        assertTrue("departmentList is not empty", departments.size() == 0);
    }

    @Test
    public void testNotIgnoreDeletedElement() throws XSDException {
        XMLBuilderHelper xmlBuilder = new XMLBuilderHelper();
        // create new department Element with name 'finance' and change-deleted processing instruction
        Element finance = xmlBuilder.createDepartment("finance", "The Hague");
        ChangeTrackingUtils.markDeleted(finance);

        // create new DepartmentList Root Element with changtracking off and initial finance department
        // with deleted processing instruction. With changeTracking off this should still be processed
        xmldcDepartmentList = xmlBuilder.buildDepartmentXMLDCElement(Collections.singletonList(finance), false);
        departments = (XMLDCCollection) xmldcDepartmentList.get("Department");
        departmentSales = null;

        assertEquals("departmentList should contain one item", 1, departments.size());
        assertEquals("finance", departments.get(0).get("name"));
        assertEquals("The Hague", departments.get(0).get("location"));
    }

}
