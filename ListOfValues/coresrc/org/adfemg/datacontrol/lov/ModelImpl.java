package org.adfemg.datacontrol.lov;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.el.ValueExpression;

import javax.faces.context.FacesContext;

import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.model.AttributeCriterion;
import oracle.adf.view.rich.model.ListOfValuesModel;
import oracle.adf.view.rich.model.QueryDescriptor;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.ViewCriteria;
import oracle.jbo.ViewObject;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.myfaces.trinidad.model.CollectionModel;
import org.apache.myfaces.trinidad.model.RowKeySet;


public class ModelImpl extends ListOfValuesModel {

    private static final ADFLogger logger = ADFLogger.createADFLogger(ModelImpl.class);
    private static final String KEY_AUTO_COMPLETE = ModelImpl.class.getName() + "_autoComplete";

    private final QueryDescriptorImpl queryDescriptor;
    private final QueryModelImpl queryModel;
    private final TableModelImpl tableModel;
    private final Map<String, ValueExpression> selectMap = new HashMap<String, ValueExpression>();
    private final Map<String, ValueExpression> lookups = new HashMap<String, ValueExpression>();

    private Map<String, Object> lastLookupVals = null;
    private Map<String, Object> lastLookupRow = null;

    public ModelImpl(final TableModelImpl tableModel, final QueryModelImpl queryModel,
                     final QueryDescriptorImpl queryDescriptor) {
        this.tableModel = tableModel;
        this.queryModel = queryModel;
        this.queryDescriptor = queryDescriptor;
    }

    /**
     * Gets a QueryDescriptor required to render the criteria part of a query
     * component. A {@code null} value indicates that a query component is
     * not used for searching a value among a list of values.
     * @return a QueryDescriptor instance. Usually the selected QueryDescriptor
     */
    @Override
    public QueryDescriptorImpl getQueryDescriptor() {
        return queryDescriptor;
    }

    /**
     * Returns a QueryModel required by a query component. A {@code null}
     * value indicates that a query component is not required for searching a
     * value among a list of values.
     * @return a QueryModel instance.
     */
    @Override
    public QueryModelImpl getQueryModel() {
        return queryModel;
    }

    @Override
    public TableModelImpl getTableModel() {
        return tableModel;
    }

    @Override
    public List<? extends Object> getItems() {
        throw new UnsupportedOperationException("ListOfValuesModelImpl.getItems");
    }

    @Override
    public List<? extends Object> getRecentItems() {
        throw new UnsupportedOperationException("ListOfValuesModelImpl.getRecentItems");
    }

    @Override
    public boolean isAutoCompleteEnabled() {
        throw new UnsupportedOperationException("ListOfValuesModelImpl.isAutoCompleteEnabled");
    }

    @Override
    public void performQuery(final QueryDescriptor queryDescriptor) {
        if (!(queryDescriptor instanceof QueryDescriptorImpl)) {
            throw new UnsupportedOperationException("ListOfValuesModelImpl.performQuery needs a QueryDescriptorImpl");
        }
        ((QueryDescriptorImpl) queryDescriptor).performQuery();
    }

    /**
     * Called by the framework during Apply Request values phase.<br/>
     *
     * This method is called only when autoSubmit is set to true on the component
     * and user either presses enter key or tab key on the input field. The method
     * determines the number of matches the parameter 'value' has in the list of
     * values .<br/>
     * Returns null, if no match was found, otherwise returns a List of rows that
     * match the value. In the case of an exact match, the List contains a single
     * entry.The type of object returned is left to the discretion of the model
     * implementor, but it should be noted that in the case of a single match,
     * the value obtained from list.get(0) is passed to the getValueFromSelection()
     * and valueSelected() methods.
     * @param value the user entered value in the input field
     * @return a List of Object instances
     */
    @Override
    public List<Object> autoCompleteValue(final Object value) {
        logger.fine("autoComplete for {0}", value);
        // markeer dit request als autoComplete request
        ADFContext.getCurrent().getRequestScope().put(KEY_AUTO_COMPLETE, Boolean.TRUE);
        final AttributeCriterion autoCrit = queryDescriptor.getAutoCompleteCriterion();
        if (autoCrit == null) { // geen autocomplete-attribuut dus gewone LOV laten tonen
            return Collections.emptyList();
        }
        final List critVals = autoCrit.getValues();
        if (autoCrit.getOperator().getOperandCount() != 1) {
            throw new IllegalArgumentException("autoComplete needs an attribuut with as default operator a single-value.");
        }
        critVals.set(0, value);
        performQuery(queryDescriptor);
        final ViewObject vo = tableModel.getViewOject();
        logger.fine("autoComplete found {0} row(s).", vo.getRowCount());
        if (vo.getRowCount() == 1) { // If we find 1 row, return this.
            final Object row = vo.getRowAtRangeIndex(0);
            if (row instanceof Row && !(row instanceof Map)) { // Wrap as Map, because this is expected by valueSelected
                final Object rowMap = new RowMapAdapter((Row) row);
                return Collections.singletonList(rowMap);
            }
            return Collections.singletonList(row);
        } else { // show LOV.
            return Collections.emptyList();
        }
    }

    public Map<String, Object> getLookup() {
        if (lookups.isEmpty()) {
            logger.warning("lookup not availeble without lookup-mappings");
            return Collections.emptyMap();
        }
        // Get the values of the attribute-bindings to be looked up in the collection.
        final Map<String, Object> searchVals = new HashMap<String, Object>();
        for (Map.Entry<String, ValueExpression> entry : lookups.entrySet()) {
            final ValueExpression valExpr = entry.getValue();
            final Object bindingVal = valExpr.getValue(FacesContext.getCurrentInstance().getELContext());
            searchVals.put(entry.getKey(), bindingVal);
        }

        // Cache for better performance by repeating the same lookup.
        if (searchVals.equals(this.lastLookupVals)) {
            return lastLookupRow;
        }
        // Iterate through the ViewObject rows to find the lookup-row.
        final ViewObject vo = getTableModel().getViewOject();
        final ViewCriteria oldCriteria = vo.getViewCriteria();
        vo.applyViewCriteria(vo.createViewCriteria());
        Map<String, Object> retval = null;
        try {
            final RowSetIterator iter = vo.createRowSetIterator("__lookup__");
            iter.setRowValidation(false);
            try {
                while (iter.hasNext()) {
                    final Row row = iter.next();
                    boolean isLookup = true;
                    // Compare the values of all the lookup attributes.
                    for (String tableAttrName : lookups.keySet()) {
                        final Object rowVal = row.getAttribute(tableAttrName);
                        final Object bindingVal = searchVals.get(tableAttrName);
                        if (!new EqualsBuilder().append(rowVal,
                                                        bindingVal).isEquals()) { // Difference found, we stop the compare.
                            isLookup = false;
                            break;
                        }
                    }
                    if (isLookup) {
                        retval = new RowMapAdapter(row) {
                            @Override
                            public Object put(final String key, final Object value) { // Ignore the put operation, because the lookup row is always
                                // read only and we don't want to put.
                                return null;
                            }
                        };
                    }
                }
            } finally {
                iter.closeRowSetIterator();
            }
        } finally {
            vo.applyViewCriteria(oldCriteria);
        }
        if (retval == null) { // Row not found.
            retval = Collections.emptyMap();
        }
        lastLookupVals = searchVals;
        // JSF also tries to write to this map because we typically use this as value
        // property #{scope.bean.lookup.attribute} of an af:inputListOfValues.
        // When selecting a value in the LOV, JSF will try to write to this field.
        // This is not needed because we already do this in the mapping.
        // @see mapCOlumnToBinding.
        lastLookupRow = new PutIgnoringMap<String, Object>(retval);
        return retval;
    }

    public static boolean isAutoCompleteRequest() {
        return Boolean.TRUE.equals(ADFContext.getCurrent().getRequestScope().get(KEY_AUTO_COMPLETE));
    }

    /**
     * This method is called by the framework at the end of the Invoke Application
     * phase, to set parameter 'value' as the selected value in the list.
     *
     * This method sets up the model such that results/related fields
     * could show the relevant data. Also, this method results in
     * value to be added to the most-recently-used or favorites list.
     * @param value the value can be
     * - a List&ltObject&gt of size 1, where Object belongs to list returned by
     * the call to getItems() (or getRecentItems()) or, <br/>
     * - a RowKeySet, containing the rowKeys of the selected rows  belonging to the
     * getTableModel().getCollectionModel(). <br/>
     * - or Object, which happens to be the single matched row, returned by call
     * to autoCompleteValue()
     */
    @Override
    public void valueSelected(final Object value) {
        if (value instanceof RowKeySet) { // Row in the LOV result-table is selected.
            logger.fine("Row(s) in LOV selected {0}", value);
            final Map row = findSelectedRow((RowKeySet) value);
            rowSelected(row);
        } else if (value instanceof Map) { // Found a single row with autoComplete
            logger.fine("Singel row (automaticly) selected {0}", value);
            rowSelected((Map) value);
        } else {
            throw new IllegalArgumentException("ListOfValuesModelImpl.valueSelected not support for " +
                                               (value == null ? null : value.getClass()));
        }
    }

    public void rowSelected(final Map row) {
        if (selectMap.isEmpty()) {
            throw new IllegalStateException("rowSelected while columnMappings are empty.");
        }
        for (Map.Entry<String, ValueExpression> map : selectMap.entrySet()) {
            final String tableAttribute = map.getKey();
            final ValueExpression valExpr = map.getValue();
            if (tableAttribute == null || valExpr == null) {
                continue;
            }
            valExpr.setValue(FacesContext.getCurrentInstance().getELContext(), row.get(tableAttribute));
        }
    }

    private Map findSelectedRow(final RowKeySet singleRowKeySet) {
        if (singleRowKeySet.getSize() != 1) {
            throw new IllegalArgumentException("singleRowKeySet must contain exactly 1 key.");
        }
        final Object key = singleRowKeySet.iterator().next();

        final CollectionModel coll = tableModel.getCollectionModel();
        final Object oldKey = coll.getRowKey();
        try {
            coll.setRowKey(key);
            return (Map) coll.getRowData();
        } finally { // restore current row
            coll.setRowKey(oldKey);
        }
    }

    public void mapColumnToBinding(final String tableAttribute, final String attrValueExpression) {
        mapColumnToBinding(tableAttribute, attrValueExpression, false);
    }

    public void mapColumnToBinding(final String tableAttribute, final String attrValueExpression,
                                   final boolean lookup) {
        final FacesContext fctx = FacesContext.getCurrentInstance();
        ValueExpression valExpr =
            fctx.getApplication().getExpressionFactory().createValueExpression(fctx.getELContext(), attrValueExpression,
                                                                               Object.class);
        selectMap.put(tableAttribute, valExpr);
        if (lookup) {
            lookups.put(tableAttribute, valExpr);
        }
    }

}
