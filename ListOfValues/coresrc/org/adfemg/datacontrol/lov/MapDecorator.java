package org.adfemg.datacontrol.lov;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class MapDecorator<K, V> implements Map<K, V> {
    private final Map<K, V> map;

    public MapDecorator(final Map<K, V> map) {
        this.map = map;
    }

    public int size() {
        return map.size();
    }

    public boolean isEmpty() {
        return map.isEmpty();
    }

    public boolean containsKey(final Object key) {
        return map.containsKey(key);
    }

    public boolean containsValue(final Object value) {
        return map.containsValue(value);
    }

    public V get(final Object key) {
        return map.get(key);
    }

    public V put(final K key, final V value) {
        return map.put(key, value);
    }

    public V remove(final Object key) {
        return map.remove(key);
    }

    public void putAll(final Map<? extends K, ? extends V> m) {
        map.putAll(m);
    }

    public void clear() {
        map.clear();
    }

    public Set<K> keySet() {
        return map.keySet();
    }

    public Collection<V> values() {
        return map.values();
    }

    public Set<Map.Entry<K, V>> entrySet() {
        return map.entrySet();
    }

}
