package org.adfemg.datacontrol.lov;

import java.util.Map;


public class PutIgnoringMap<K, V> extends MapDecorator<K, V> {
    public PutIgnoringMap(final Map map) {
        super(map);
    }

    @Override
    public V put(final K key, final V value) {
        // ignore put
        return get(key);
    }
}
