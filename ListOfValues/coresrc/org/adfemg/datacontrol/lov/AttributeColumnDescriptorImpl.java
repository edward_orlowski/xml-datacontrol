package org.adfemg.datacontrol.lov;

import java.util.Collections;
import java.util.Set;

import javax.faces.convert.Converter;

import oracle.adf.view.rich.model.AttributeDescriptor;
import oracle.adf.view.rich.model.ColumnDescriptor;


public class AttributeColumnDescriptorImpl extends ColumnDescriptor {

    public static final int DFLT_WIDTH = 40;

    private int width = DFLT_WIDTH;
    private String align;
    private final AttributeDescriptor attribute;

    /**
     * Constructor with mandatory field attribute.
     * @param attribute the AttributeDescriptor.
     */
    public AttributeColumnDescriptorImpl(final AttributeDescriptor attribute) {
        this.attribute = attribute;
    }

    public void setWidth(final int width) {
        this.width = width;
    }

    @Override
    public int getWidth() {
        return width;
    }

    public void setAlign(final String align) {
        this.align = align;
    }

    @Override
    public String getAlign() {
        return align;
    }

    @Override
    public AttributeDescriptor.ComponentType getComponentType() {
        return attribute.getComponentType();
    }

    @Override
    public String getDescription() {
        return attribute.getDescription();
    }

    @Override
    public String getLabel() {
        return attribute.getLabel();
    }

    @Override
    public int getLength() {
        return attribute.getLength();
    }

    @Override
    public int getMaximumLength() {
        return attribute.getMaximumLength();
    }

    @Override
    public String getName() {
        return attribute.getName();
    }

    @Override
    public Set<AttributeDescriptor.Operator> getSupportedOperators() {
        return Collections.unmodifiableSet(attribute.getSupportedOperators());
    }

    @Override
    public Class getType() {
        return attribute.getType();
    }

    @Override
    public boolean isReadOnly() {
        return attribute.isReadOnly();
    }

    @Override
    public boolean isRequired() {
        return attribute.isRequired();
    }

    @Override
    public boolean isIndexed() {
        return attribute.isIndexed();
    }

    @Override
    public String getFormat() {
        return attribute.getFormat();
    }

    @Override
    public Converter getConverter() {
        return attribute.getConverter();
    }

    @Override
    public boolean hasDefaultConverter() {
        return attribute.hasDefaultConverter();
    }

    @Override
    public Object getModel() {
        return attribute.getModel();
    }

}
