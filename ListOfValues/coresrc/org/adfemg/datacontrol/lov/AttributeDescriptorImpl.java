package org.adfemg.datacontrol.lov;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.model.AttributeDescriptor;

import oracle.jbo.AttributeDef;
import oracle.jbo.AttributeHints;
import oracle.jbo.LocaleContext;
import oracle.jbo.common.JboCompOper;


public class AttributeDescriptorImpl extends AttributeDescriptor {

    private static final ADFLogger logger = ADFLogger.createADFLogger(AttributeDescriptorImpl.class);

    //FIXME: Internationalize the operators?! We prob. want to support both English, Dutch, ... ?
    //finals voor supported operators
    private static final String BEGINT_MET = "begint met";
    private static final String EINDIGT_OP = "eindigt op";
    private static final String GELIJK_AAN = "gelijk aan";
    private static final String NIET_GELIJK_AAN = "niet gelijk aan";
    private static final String KLEINER_DAN = "kleiner dan";
    private static final String BEVAT = "bevat";
    private static final String BEVAT_NIET = "bevat niet";
    private static final String IS_LEEG = "is leeg";
    private static final String GROTER_DAN = "groter dan";
    private static final String KLEINER_OF_GELIJK_AAN = "kleiner of gelijk aan";
    private static final String GROTER_OF_GELIJK_AAN = "groter of gelijk aan";
    private static final String TUSSEN = "tussen";
    private static final String NIET_TUSSEN = "niet tussen";
    private static final String IS_NIET_LEEG = "is niet leeg";
    private static final String VOOR = "voor";
    private static final String NA = "na";
    private static final String OP_OF_VOOR = "op of voor";
    private static final String OP_OF_NA = "op of na";

    private final String name;
    private final Class<?> type;
    private final AttributeDescriptor.ComponentType componentType;
    private String description;
    private final String label;
    private int length = 0;
    private int maximumLength = 0; // 0 = no maximum
    private final Set<AttributeDescriptor.Operator> supportedOperators =
        new LinkedHashSet<AttributeDescriptor.Operator>();
    private boolean readOnly = false;
    private boolean required = false;
    private boolean indexed = false;
    private String format;
    private Object model;


    /**
     * The constructor with the mandatory fields.
     *
     * @param jboAttribute
     * @param localeContext
     */
    public AttributeDescriptorImpl(final AttributeDef jboAttribute, final LocaleContext localeContext) {
        this.name = jboAttribute.getName();
        final AttributeHints hints = jboAttribute.getUIHelper();
        this.label = hints.getLabel(localeContext);
        this.type = jboAttribute.getJavaType();

        //Create the standard support for the following operators.
        if (isString()) {
            supportedOperators.add(new OperatorImpl(BEGINT_MET, JboCompOper.OPER_STARTS_WITH, 1));
            supportedOperators.add(new OperatorImpl(EINDIGT_OP, JboCompOper.OPER_ENDS_WITH, 1));
            supportedOperators.add(new OperatorImpl(GELIJK_AAN, JboCompOper.OPER_EQ, 1));
            supportedOperators.add(new OperatorImpl(NIET_GELIJK_AAN, JboCompOper.OPER_NE, 1));
            supportedOperators.add(new OperatorImpl(KLEINER_DAN, JboCompOper.OPER_LT, 1));
            supportedOperators.add(new OperatorImpl(GROTER_DAN, JboCompOper.OPER_GT, 1));
            supportedOperators.add(new OperatorImpl(KLEINER_OF_GELIJK_AAN, JboCompOper.OPER_LE, 1));
            supportedOperators.add(new OperatorImpl(GROTER_OF_GELIJK_AAN, JboCompOper.OPER_GE, 1));
            supportedOperators.add(new OperatorImpl(TUSSEN, JboCompOper.OPER_BETWEEN, 2));
            supportedOperators.add(new OperatorImpl(NIET_TUSSEN, JboCompOper.OPER_NOT_BETWEEN, 2));
            supportedOperators.add(new OperatorImpl(BEVAT, JboCompOper.OPER_CONTAINS, 1));
            supportedOperators.add(new OperatorImpl(BEVAT_NIET, JboCompOper.OPER_DOES_NOT_CONTAIN, 1));
            supportedOperators.add(new OperatorImpl(IS_LEEG, JboCompOper.OPER_IS_BLANK, 0));
            supportedOperators.add(new OperatorImpl(IS_NIET_LEEG, JboCompOper.OPER_IS_NOT_BLANK, 0));
            componentType = AttributeDescriptor.ComponentType.inputText;
        } else if (isNumeric()) {
            supportedOperators.add(new OperatorImpl(GELIJK_AAN, JboCompOper.OPER_EQ, 1));
            supportedOperators.add(new OperatorImpl(NIET_GELIJK_AAN, JboCompOper.OPER_NE, 1));
            supportedOperators.add(new OperatorImpl(KLEINER_DAN, JboCompOper.OPER_LT, 1));
            supportedOperators.add(new OperatorImpl(KLEINER_OF_GELIJK_AAN, JboCompOper.OPER_LE, 1));
            supportedOperators.add(new OperatorImpl(GROTER_DAN, JboCompOper.OPER_GT, 1));
            supportedOperators.add(new OperatorImpl(GROTER_OF_GELIJK_AAN, JboCompOper.OPER_GE, 1));
            supportedOperators.add(new OperatorImpl(TUSSEN, JboCompOper.OPER_BETWEEN, 2));
            supportedOperators.add(new OperatorImpl(NIET_TUSSEN, JboCompOper.OPER_NOT_BETWEEN, 2));
            supportedOperators.add(new OperatorImpl(IS_LEEG, JboCompOper.OPER_IS_BLANK, 0));
            supportedOperators.add(new OperatorImpl(IS_NIET_LEEG, JboCompOper.OPER_IS_NOT_BLANK, 0));
            componentType = AttributeDescriptor.ComponentType.inputNumberSpinbox;
        } else if (isDate()) {
            supportedOperators.add(new OperatorImpl(GELIJK_AAN, JboCompOper.OPER_EQ, 1));
            supportedOperators.add(new OperatorImpl(NIET_GELIJK_AAN, JboCompOper.OPER_NE, 1));
            supportedOperators.add(new OperatorImpl(VOOR, JboCompOper.OPER_BEFORE, 1));
            supportedOperators.add(new OperatorImpl(NA, JboCompOper.OPER_AFTER, 1));
            supportedOperators.add(new OperatorImpl(OP_OF_VOOR, JboCompOper.OPER_ON_OR_BEFORE, 1));
            supportedOperators.add(new OperatorImpl(OP_OF_NA, JboCompOper.OPER_ON_OR_AFTER, 1));
            supportedOperators.add(new OperatorImpl(TUSSEN, JboCompOper.OPER_BETWEEN, 2));
            supportedOperators.add(new OperatorImpl(NIET_TUSSEN, JboCompOper.OPER_NOT_BETWEEN, 2));
            supportedOperators.add(new OperatorImpl(IS_LEEG, JboCompOper.OPER_IS_BLANK, 0));
            supportedOperators.add(new OperatorImpl(IS_NIET_LEEG, JboCompOper.OPER_IS_NOT_BLANK, 0));
            componentType = AttributeDescriptor.ComponentType.inputDate;
        } else {
            logger.warning("Unknown datatype " + type);
            componentType = AttributeDescriptor.ComponentType.inputText;
        }
    }

    @Override
    public AttributeDescriptor.ComponentType getComponentType() {
        return componentType;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getLabel() {
        return label;
    }

    public void setLength(final int length) {
        this.length = length;
    }

    @Override
    public int getLength() {
        return length;
    }

    public void setMaximumLength(final int maximumLength) {
        this.maximumLength = maximumLength;
    }

    @Override
    public int getMaximumLength() {
        return maximumLength;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Set<AttributeDescriptor.Operator> getSupportedOperators() {
        return Collections.unmodifiableSet(supportedOperators);
    }

    public void addSupportedOperator(final OperatorImpl operator) {
        supportedOperators.add(operator);
    }

    public void removeSupportedOperators() {
        supportedOperators.clear();
    }

    @Override
    public Class getType() {
        return type;
    }

    public void setReadOnly(final boolean readOnly) {
        this.readOnly = readOnly;
    }

    @Override
    public boolean isReadOnly() {
        return readOnly;
    }

    public void setRequired(final boolean required) {
        this.required = required;
    }

    @Override
    public boolean isRequired() {
        return required;
    }

    public void setIndexed(final boolean indexed) {
        this.indexed = indexed;
    }

    @Override
    public boolean isIndexed() {
        return indexed;
    }

    public void setFormat(final String format) {
        this.format = format;
    }

    @Override
    public String getFormat() {
        return format;
    }

    @Override
    public Object getModel() {
        return model;
    }

    public final boolean isString() {
        return String.class.isAssignableFrom(type);
    }

    public final boolean isNumeric() {
        return Number.class.isAssignableFrom(type);
    }

    public final boolean isDate() {
        return java.util.Date.class.isAssignableFrom(type);
    }

    public class OperatorImpl extends AttributeDescriptor.Operator {
        private final String label;
        private final String value;
        private final int operandCount;

        public OperatorImpl(final String label, final String value, final int operandCount) {
            this.label = label;
            this.value = value;
            this.operandCount = operandCount;
        }

        public String getLabel() {
            return label;
        }

        public String getValue() {
            return value;
        }

        public int getOperandCount() {
            return operandCount;
        }
    }

}
