package org.adfemg.datacontrol.lov;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import oracle.adf.view.rich.model.AttributeDescriptor;
import oracle.adf.view.rich.model.QueryDescriptor;
import oracle.adf.view.rich.model.QueryModel;


public class QueryModelImpl extends QueryModel {

    private final List<AttributeDescriptor> attributes = new ArrayList<AttributeDescriptor>();

    /**
     * No-arg default constructor.
     */
    public QueryModelImpl() {
        super();
    }

    public QueryModelImpl(final List<AttributeDescriptor> attributes) {
        this.attributes.addAll(attributes);
    }

    @Override
    public List<AttributeDescriptor> getAttributes() {
        // These attributes can be added in the advanced mode.
        return Collections.unmodifiableList(attributes);
    }

    @Override
    public QueryDescriptor create(final String name, final QueryDescriptor qdBase) {
        throw new UnsupportedOperationException("QueryModelImpl.create");
    }

    @Override
    public void delete(final QueryDescriptor qd) {
        throw new UnsupportedOperationException("QueryModelImpl.delete");
    }

    @Override
    public List<QueryDescriptor> getSystemQueries() {
        throw new UnsupportedOperationException("QueryModelImpl.getSystemQueries");
    }

    @Override
    public List<QueryDescriptor> getUserQueries() {
        throw new UnsupportedOperationException("QueryModelImpl.getUserQueries");
    }

    /**
     * Resets the QueryDescriptor to its last saved state.
     *
     * <p>
     *   This method is invoked during the 'Invoke Application' phase of the JSF lifecyle.
     *   Subclasses can override this method to reset the QueryDescriptor to its original state.
     * </p>
     *
     * @param qd  QueryDescriptor to be restored to its last saved state.
     */
    @Override
    public void reset(final QueryDescriptor qd) {
        // Gets called when the LOV is opened and when the user explicite presses the reset button.
        if (qd instanceof QueryDescriptorImpl) {
            ((QueryDescriptorImpl) qd).reset();
        }
    }

    @Override
    public void setCurrentDescriptor(final QueryDescriptor qd) {
        throw new UnsupportedOperationException("QueryModelImpl.setCurrentDescriptor");
    }

    @Override
    public void update(final QueryDescriptor qd, final Map<String, Object> uiHints) {
        throw new UnsupportedOperationException("QueryModelImpl.update");
    }

}
