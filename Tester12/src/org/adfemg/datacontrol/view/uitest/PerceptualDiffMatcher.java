package org.adfemg.datacontrol.view.uitest;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

public class PerceptualDiffMatcher extends TypeSafeMatcher<TakesScreenshot> {

    private final Path referenceImage;
    private Path screenshot;
    private Path difference;

    public PerceptualDiffMatcher(final Path referenceImage) {
        this.referenceImage = referenceImage;
    }

    @Override
    protected boolean matchesSafely(TakesScreenshot driver) {
        System.out.println("TO DO: compare screenshot");
        screenshot = siblingWithSuffix(referenceImage, "-actual");
        difference = siblingWithSuffix(referenceImage, "-diff");
        final byte[] bytes = driver.getScreenshotAs(OutputType.BYTES);
        try {
            Files.copy(new ByteArrayInputStream(bytes), screenshot, StandardCopyOption.REPLACE_EXISTING);
            List<String> args = new ArrayList<>();
            args.add(Paths.get("").resolve("..").resolve("tools").resolve("ImageMagick-6.9.2-3-portable-Q16-x64").resolve("compare.exe").normalize().toAbsolutePath().toString());
            args.add("-verbose");
            args.add("-metric");
            args.add("RMSE");
            args.add("-highlight-color");
            args.add("Red");
            //args.add("-compose");
            //args.add("Src");
            args.add(referenceImage.normalize().toAbsolutePath().toString());
            args.add(screenshot.normalize().toAbsolutePath().toString());
            args.add(difference.normalize().toAbsolutePath().toString());
            Process exec = Runtime.getRuntime().exec(args.toArray(new String[0]));
            int result = exec.waitFor();
            if (result == 0) {
                Files.deleteIfExists(screenshot);
                Files.deleteIfExists(difference);
                screenshot = null;
                difference = null;
                return true;
            } else if (result == 1) {
                // images are different
                return false;
            } else {
                // unknown error occured
                Files.deleteIfExists(difference);
                difference = null;
                return false;
            }
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private Path siblingWithSuffix(final Path base, final String suffix) {
        String filename = base.getFileName().toString();
        String basename = filename.substring(0, filename.lastIndexOf("."));
        String extension = filename.substring(filename.lastIndexOf(".") + 1);
        Path retval = base.resolveSibling(basename + suffix + "." + extension);
        System.out.println(retval);
        return retval;
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("a screenshot visually similar to ").appendText(referenceImage.toAbsolutePath().normalize().toString());
    }

    @Override
    protected void describeMismatchSafely(TakesScreenshot driver, Description mismatchDescription) {
        mismatchDescription.appendText("was screenshot saved at ");
        mismatchDescription.appendText(screenshot.toAbsolutePath().normalize().toString());
        if (difference != null) {
            mismatchDescription.appendText(" with differences marked in ");
            mismatchDescription.appendText(difference.toAbsolutePath().normalize().toString());
        }
    }

    @Factory
    public static Matcher<TakesScreenshot> hasSimilarScreenshot(Path referenceImage) {
        return new PerceptualDiffMatcher(referenceImage);
    }
}
