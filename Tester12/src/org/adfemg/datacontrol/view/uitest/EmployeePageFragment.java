package org.adfemg.datacontrol.view.uitest;

import com.redheap.selenium.component.AdfButton;
import com.redheap.selenium.component.AdfInputDate;
import com.redheap.selenium.component.AdfInputText;
import com.redheap.selenium.component.AdfRegion;
import com.redheap.selenium.component.AdfSelectBooleanCheckbox;
import com.redheap.selenium.page.PageFragment;

public class EmployeePageFragment extends PageFragment {

    private static final String ID_EMPLOYEE_ID = "it1";
    private static final String ID_FIRST_NAME = "it2";
    private static final String ID_LAST_NAME = "it3";
    private static final String ID_EMAIL = "it4";
    private static final String ID_PHONE_NUMBER = "it5";
    private static final String ID_HIRE_DATE = "id1";
    private static final String ID_SALARY = "it6";
    private static final String ID_COMMISSION = "it7";
    private static final String ID_SHOW_JOB_INFO = "sbc1";
    private static final String ID_JOB_ID = "it8";
    private static final String ID_JOB_TITLE = "it9";
    private static final String ID_JOB_MIN_SALARY = "it10";
    private static final String ID_JOB_MAX_SALARY = "it11";
    private static final String ID_BUTTON_FIRST = "b1";
    private static final String ID_BUTTON_PREVIOUS = "b2";
    private static final String ID_BUTTON_NEXT = "b3";
    private static final String ID_BUTTON_LAST = "b4";
    private static final String ID_BUTTON_SUBMIT = "b5";

    public EmployeePageFragment(AdfRegion region) {
        super(region);
    }

    public AdfInputText getId() {
        return findAdfComponent(ID_EMPLOYEE_ID);
    }

    public AdfInputText getFirstName() {
        return findAdfComponent(ID_FIRST_NAME);
    }

    public AdfInputText getLastName() {
        return findAdfComponent(ID_LAST_NAME);
    }

    public AdfInputText getEmail() {
        return findAdfComponent(ID_EMAIL);
    }

    public AdfInputText getPhoneNumber() {
        return findAdfComponent(ID_PHONE_NUMBER);
    }

    public AdfInputDate getHireDate() {
        return findAdfComponent(ID_HIRE_DATE);
    }

    public AdfInputText getSalary() {
        return findAdfComponent(ID_SALARY);
    }

    public AdfInputText getCommissionPercentage() {
        return findAdfComponent(ID_COMMISSION);
    }

    public AdfSelectBooleanCheckbox getShowJobInfo() {
        return findAdfComponent(ID_SHOW_JOB_INFO);
    }

    public AdfInputText getJobId() {
        return findAdfComponent(ID_JOB_ID);
    }

    public AdfInputText getJobTitle() {
        return findAdfComponent(ID_JOB_TITLE);
    }

    public AdfInputText getJobMinSalary() {
        return findAdfComponent(ID_JOB_MIN_SALARY);
    }

    public AdfInputText getJobMaxSalary() {
        return findAdfComponent(ID_JOB_MAX_SALARY);
    }

    public AdfButton getFirstButton() {
        return findAdfComponent(ID_BUTTON_FIRST);
    }

    public AdfButton getPreviousButton() {
        return findAdfComponent(ID_BUTTON_PREVIOUS);
    }

    public AdfButton getNextButton() {
        return findAdfComponent(ID_BUTTON_NEXT);
    }

    public AdfButton getLastButton() {
        return findAdfComponent(ID_BUTTON_LAST);
    }

    public AdfButton getSubmitButton() {
        return findAdfComponent(ID_BUTTON_SUBMIT);
    }

}
