package org.adfemg.datacontrol.view.uitest;

import java.io.IOException;

import java.nio.file.Path;
import java.nio.file.Paths;

import java.util.Collections;
import java.util.List;

import org.jacoco.core.tools.ExecDumpClient;
import org.jacoco.core.tools.ExecFileLoader;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

public class JacocoRule implements TestRule {

    private final boolean reset;
    private final boolean append;
    private final String host;
    private final int port;
    private final int retryCount;
    private final long retryDelay;
    // FIXME rename to dumpdir??
    private final Path outputdir;
    //private final boolean report;
    private final JacocoReporter reporter;

    private Description description;

    public static Builder builder() {
        return new Builder();
    }

    private JacocoRule(final String host, final int port, final int retryCount, final long retryDelay,
                       final boolean reset, final boolean append, final Path outputdir, final JacocoReporter reporter
                       ) {
        this.host = host;
        this.port = port;
        this.retryCount = retryCount;
        this.retryDelay = retryDelay;
        this.reset = reset;
        this.append = append;
        this.outputdir = outputdir;
        this.reporter = reporter;
    }

    private void before() throws IOException {
        if (reset) {
            reset();
        }
    }

    private void after() throws IOException {
        ExecDumpClient client = new ExecDumpClient();
        ExecFileLoader dump = dump(client);
        Path file = getDumpFile();
        System.out.println("*** DUMPING " + description.getDisplayName() + " TO " + file);
        dump.save(file.toFile(), append);
        if (reporter != null) {
            reporter.report(dump, description);
        }
    }

    protected void reset() throws IOException {
        ExecDumpClient client = new ExecDumpClient();
        client.setDump(false);
        client.setReset(true);
        System.out.println("*** RESETTING JACOCO " + description.getDisplayName());
        dump(client);
    }

    protected ExecFileLoader dump(ExecDumpClient client) throws IOException {
        client.setRetryCount(retryCount);
        client.setRetryDelay(retryDelay);
        return client.dump(host, port);
    }


    protected Path getDumpFile() throws IOException {
        return outputdir.resolve(getDecriptionFileName() + ".exec").normalize().toAbsolutePath();
    }

    protected String getDecriptionFileName() {
        StringBuilder filename = new StringBuilder(description.getClassName());
        if (description.getMethodName() != null) {
            filename.append("-").append(description.getMethodName());
        }
        return filename.toString();
    }

    @Override
    public Statement apply(final Statement base, final Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                JacocoRule.this.description = description;
                before();
                try {
                    base.evaluate();
                } finally {
                    after();
                }
            }
        };
    }

    public static class Builder {
        private boolean reset = true;
        private boolean append = false;
        private String host = "localhost";
        private int port = 6300;
        private int retryCount = 0;
        private long retryDelay = 1000;
        private Path outputdir = Paths.get(".");
        private JacocoReporter reporter = null;

        private static final List<Path> DFLT_PATH = Collections.singletonList(Paths.get("."));

        protected Builder() {
        }

        public Builder withoutReset() {
            this.reset = false;
            return this;
        }

        public Builder withAppend() {
            this.append = true;
            return this;
        }

        public Builder withHost(final String host) {
            this.host = host;
            return this;
        }

        public Builder withPort(final int port) {
            this.port = port;
            return this;
        }

        public Builder withRetryCount(final int retryCount) {
            this.retryCount = retryCount;
            return this;
        }

        public Builder withRetryDelay(final long retryDelay) {
            this.retryDelay = retryDelay;
            return this;
        }

        public Builder withOutputDir(final Path dir) {
            this.outputdir = dir;
            return this;
        }

        public Builder withReporter(final JacocoReporter reporter) {
            this.reporter = reporter;
            return this;
        }

        public JacocoRule build() {
            return new JacocoRule(host, port, retryCount, retryDelay, reset, append, outputdir, reporter);
        }
    }

}
