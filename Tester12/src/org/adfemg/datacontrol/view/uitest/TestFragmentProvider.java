package org.adfemg.datacontrol.view.uitest;

import com.redheap.selenium.junit.PageProvider;
import com.redheap.selenium.page.PageFragment;

import org.junit.rules.ExternalResource;

import org.openqa.selenium.WebDriver;

public class TestFragmentProvider<F extends PageFragment> extends ExternalResource {

    private final PageProvider<TaskflowTester> tftester;
    private final Class<F> fragmentClass;

    public TestFragmentProvider(Class<F> fragmentClass, String contextUrl, WebDriver driver) {
        this.tftester = new PageProvider<TaskflowTester>(TaskflowTester.class, contextUrl + "/faces/tftester", driver);
        this.fragmentClass = fragmentClass;
    }

    public F selectFromTree(final String taskFlow, final String testCase) {
        TaskflowTester tester = tftester.goHome();
        return tester.runTest(taskFlow, testCase, fragmentClass);
    }

}
