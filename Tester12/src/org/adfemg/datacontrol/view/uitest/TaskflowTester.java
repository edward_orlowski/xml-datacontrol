package org.adfemg.datacontrol.view.uitest;

import com.redheap.selenium.AdfFinder;
import com.redheap.selenium.component.AdfCommandButton;
import com.redheap.selenium.component.AdfRegion;
import com.redheap.selenium.component.AdfTree;
import com.redheap.selenium.page.Page;
import com.redheap.selenium.page.PageFragment;

import java.lang.reflect.InvocationTargetException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

public class TaskflowTester extends Page {


    private static final String taskFlowTree = "pt1:pctree:tree";
    private static final String runTestButton = "pt1:cb1";
    private static final String regionStretch = "pt1:r1";
    private static final String regionNoStretch = "pt1:r2";

    public TaskflowTester(WebDriver driver) {
        super(driver);
    }

    @Override
    protected String getExpectedTitle() {
        return "ADF EMG Task Flow Tester";
    }

    public <F extends PageFragment> F runTest(final String taskFlow, final String testCase, final Class<F> cls) {
        final AdfTree tree = findTree();
        tree.clickNode(AdfFinder.treeNodeByLabel(taskFlow));
        tree.clickNode(AdfFinder.treeNodeByLabel(testCase));
        findRunTestButton().click();
        final AdfRegion region = findRegion();
        try {
            return cls.getConstructor(AdfRegion.class).newInstance(region);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException |
                 InstantiationException e) {
            throw new WebDriverException(e);
        }
    }

    private AdfTree findTree() {
        return findAdfComponent(taskFlowTree);
    }

    private AdfCommandButton findRunTestButton() {
        return findAdfComponent(runTestButton);
    }

    private AdfRegion findRegion() {
        AdfRegion retval = findAdfComponent(regionStretch);
        if (retval == null) {
            retval = findAdfComponent(regionNoStretch);
        }
        return retval;
    }
}
