
package org.adfemg.hr;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the org.adfemg.hr package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Job_QNAME = new QName("http://adfemg.org/HR", "Job");
    private final static QName _Employee_QNAME = new QName("http://adfemg.org/HR", "Employee");
    private final static QName _Department_QNAME = new QName("http://adfemg.org/HR", "Department");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.adfemg.hr
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TDepartment }
     *
     */
    public TDepartment createTDepartment() {
        return new TDepartment();
    }

    /**
     * Create an instance of {@link TJob }
     *
     */
    public TJob createTJob() {
        return new TJob();
    }

    /**
     * Create an instance of {@link ListAllDepartmentsRequest }
     *
     */
    public ListAllDepartmentsRequest createListAllDepartmentsRequest() {
        return new ListAllDepartmentsRequest();
    }

    /**
     * Create an instance of {@link DepartmentEmployeesRequest }
     *
     */
    public DepartmentEmployeesRequest createDepartmentEmployeesRequest() {
        return new DepartmentEmployeesRequest();
    }

    /**
     * Create an instance of {@link ListAllDepartmentsResponse }
     *
     */
    public ListAllDepartmentsResponse createListAllDepartmentsResponse() {
        return new ListAllDepartmentsResponse();
    }

    /**
     * Create an instance of {@link Location }
     *
     */
    public Location createLocation() {
        return new Location();
    }

    /**
     * Create an instance of {@link TLocation }
     *
     */
    public TLocation createTLocation() {
        return new TLocation();
    }

    /**
     * Create an instance of {@link DepartmentList }
     *
     */
    public DepartmentList createDepartmentList() {
        return new DepartmentList();
    }

    /**
     * Create an instance of {@link TEmployee }
     *
     */
    public TEmployee createTEmployee() {
        return new TEmployee();
    }

    /**
     * Create an instance of {@link DepartmentEmployeesResponse }
     *
     */
    public DepartmentEmployeesResponse createDepartmentEmployeesResponse() {
        return new DepartmentEmployeesResponse();
    }

    /**
     * Create an instance of {@link TDepartment.Employees }
     *
     */
    public TDepartment.Employees createTDepartmentEmployees() {
        return new TDepartment.Employees();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TJob }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://adfemg.org/HR", name = "Job")
    public JAXBElement<TJob> createJob(TJob value) {
        return new JAXBElement<TJob>(_Job_QNAME, TJob.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TEmployee }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://adfemg.org/HR", name = "Employee")
    public JAXBElement<TEmployee> createEmployee(TEmployee value) {
        return new JAXBElement<TEmployee>(_Employee_QNAME, TEmployee.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TDepartment }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://adfemg.org/HR", name = "Department")
    public JAXBElement<TDepartment> createDepartment(TDepartment value) {
        return new JAXBElement<TDepartment>(_Department_QNAME, TDepartment.class, null, value);
    }

}
