package org.adfemg.datacontrol.extension12;

import javax.naming.Context;

import oracle.adf.model.adapter.AdapterDefinition;
import oracle.adf.model.adapter.DTContext;
import oracle.adf.share.dt.AppConnContext;

import oracle.adfdt.model.datacontrols.AdapterSettings;

import oracle.bali.xml.model.XmlContext;

import oracle.ide.Addin;
import oracle.ide.Ide;
import oracle.ide.model.Workspace;
//import oracle.adfdtinternal.model.ide.addins.AdapterContextHandler;

public class XMLDCAddin implements Addin {

    public XMLDCAddin() {
        super();
    }

    @Override
    public void initialize() {
        // TODO Implement this method
        XmlContext.registerSetupHook(new XMLDCXmlContextListener());
        // Context Handler already registered by oracle.adfm.dc-adapters dependency
        //MetaDef.registerHandler(MetaDef.CTX_JDEVELOPER, new AdapterContextHandler());

        // DataControlDefinition.setDtClassLoader(XMLDCAddin.class.getClassLoader());
        AdapterDefinition.registerAdapterDefinitions(XMLDCAddin.class.getClassLoader());
        // this does register the factory that gets called to create
        // org.adfemg.datacontrol.xml.DataControlDefinition
        // This can probably be removed and replaced with a data-control-definition-hook
        // in the extension.xml with a child <definitionImplClass class="oracle.DCDefImplClassA"/>
        // to let the Adapter DC now about our possible definition class
        AdapterSettings.addFactory(new XMLDCDataControlObjectFactory());

        DTContext.setCurrentContextImpl(new DTContext() {
            public Context getConnectionContext() throws Exception {
                AppConnContext appConnCtx = AppConnContext.getInstance();
                Workspace workspace = Ide.getActiveWorkspace();
                appConnCtx.setCurrentWorkspace(workspace);
                return appConnCtx.getConnectionContext();
            }
        });
    }

}
