# ADF XML DataControl #

The ADF XML DataControl offers a simple, yet powerful, datacontrol for Oracle's Application Development Framework (ADF) to expose XML sources as a data control. The XML could come from static files, web service calls, EL expressions, or an custom data source. The datacontrol itself is highly flexible with a plugin adapter framework and java annotations.

### See the [wiki](https://adfxmldc.atlassian.net/wiki/display/XMLDC) for more details ###

## Quick Start ##
Two quick start options are available:

* Download the JDeveloper extension and start using it in your own project. See the [5 Minute Tutorial](https://adfxmldc.atlassian.net/wiki/display/XMLDC/Getting+Started+in+5+Minutes) to get cracking

* Fork or clone the git repo and build the extension from source so you can make whatever changes you like. See the [Contributor's Guide](https://adfxmldc.atlassian.net/wiki/display/XMLDC/Contributor%27s+Guide) for more details.

## Bugs and feature requests ##
Have a bug or a feature request? Please first [search](https://adfxmldc.atlassian.net/issues/?jql=) for existing and closed issues. If your problem or idea is not yet addressed yet, please [open a new issue](https://adfxmldc.atlassian.net/secure/CreateIssue!default.jspa).

## Documentation ##
Most documentation is on the [wiki](https://adfxmldc.atlassian.net/wiki/display/XMLDC).
JavaDoc is included with the distribution so should be available within JDeveloper. We are still in the process of publishing JavaDoc on a public server. As soon as that is done, we'll include a link here.

## Contributing ##
Please read through our [contributing guidelines](https://adfxmldc.atlassian.net/wiki/display/XMLDC/Contributor%27s+Guide). Included are directions for opening issues, coding standards, and notes on development.

## Community ##
Keep track of development and community news.

* Read and subscribe to the [ADF Enterprise Methodology Group](http://www.adfemg.org)

## Creators ##
**Wilfred van der Deijl** [![ACE Director](http://www.redheap.com/favicon.ico)](http://www.oracle.com/us/community/ace-program)

* https://twitter.com/wilfreddeijl

* https://bitbucket.org/wvanderdeijl

**Richard Olrichs**

* https://twitter.com/RichardOlrichs

* https://bitbucket.org/RichardOlrichs

## Copyright and License ##
Code released under the [Apache License, version 2.0](http://www.apache.org/licenses/LICENSE-2.0.html). Feel free to use it in any other open source project, closed source, or even commercial project.