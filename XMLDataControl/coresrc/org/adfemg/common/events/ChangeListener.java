package org.adfemg.common.events;

/**
 * Defina a object that listens to the ChangeEvents.
 *
 * @see ChangeEvent
 */
public interface ChangeListener {
    /**
     * This method will be called when the Object this listener is registered to
     * will change.
     *
     * @param event het ChangeEvent
     */
    void objectChanged(ChangeEvent event);
}
