/**
 * Deprecated package with some eventig support. Some if this can be removed and other parts can be moved
 * to org.adfemg.datacontrol.xml.events if we really need it.
 */
package org.adfemg.common.events;

