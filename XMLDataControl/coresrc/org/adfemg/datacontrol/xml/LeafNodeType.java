package org.adfemg.datacontrol.xml;

/**
 * Enumeration to identify the Leaf Node Type.
 * Can be an Element, Attribute or Scalar.
 */
public enum LeafNodeType {
    ELEMENT,
    ATTRIBUTE,
    SCALAR_COLLECTION_ELEMENT;
}
