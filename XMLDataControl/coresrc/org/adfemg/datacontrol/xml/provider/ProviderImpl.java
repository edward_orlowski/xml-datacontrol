package org.adfemg.datacontrol.xml.provider;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import oracle.adf.model.adapter.AdapterException;
import oracle.adf.share.logging.ADFLogger;

import org.adfemg.datacontrol.xml.DataControl;
import org.adfemg.datacontrol.xml.provider.data.DataRequest;
import org.adfemg.datacontrol.xml.provider.typemap.TypeMapper;
import org.adfemg.datacontrol.xml.utils.XmlParseUtils;

import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import org.xml.sax.SAXException;


/**
 * Abstract class implementing the Provider.
 *
 * @see Provider
 */
public abstract class ProviderImpl implements Provider {

    private Map<String, String> stringParams = new LinkedHashMap<String, String>(4);
    private Map<String, List<String>> listParams = new LinkedHashMap<String, List<String>>(1);
    private Map<String, Node> xmlParams = new LinkedHashMap<String, Node>(1);

    // Regular expression to find #{param.xxx} in a text.
    private static final Pattern REPLACE_PATTERN =
        Pattern.compile("(.*?)(\\#\\{param\\.)([^}]*)(\\})(.*)", Pattern.DOTALL);

    // pattern groups for text before #{param.xxx}, the name of the param, and the text after #{param.xxx}
    private static final int GROUP_BEFORE_PARAM = 1;
    private static final int GROUP_PARAM_NAME = 3;
    private static final int GROUP_AFTER_PARAM = 5;

    private static final ADFLogger logger = ADFLogger.createADFLogger(ProviderImpl.class);

    /**
     * Default no-args constructor.
     */
    public ProviderImpl() {
        super();
    }

    /**
     * Invoked by DataControlDefinition whenever it instantiates a new datacontrol so each Provider of that
     * datacontrol gets a chance to do further initialization once the datacontrol is known. This default
     * implementation doesn't do anything as most providers won't probably need this. Having an empty implementation
     * is easier than declaring this abstract and forcing each provider to implement this. In the rare case that
     * a provider needs this it can still override this method and do its own initialization.
     * @param dc DataControl being created
     * @see DataControlDefinition#createDataControl
     */
    @Override
    public void dataControlCreated(final DataControl dc) {
        // NOOP
    }

    /**
     * Sets the value of a string parameter. Typically only invoked when reading the datacontrol definition from
     * DataControls.dcx and initializing the datacontrol providers.
     * @param name name of the parameter to set
     * @param value new value for the parameter
     */
    @Override
    public void setStringParameter(String name, String value) {
        stringParams.put(name, value);
    }

    /**
     * Sets the value of a list parameter. Typically only invoked when reading the datacontrol definition from
     * DataControls.dcx and initializing the datacontrol providers.
     * @param name name of the parameter to set
     * @param values list of new values for the parameter
     */
    @Override
    public void setListParameter(String name, List<String> values) {
        listParams.put(name, new ArrayList<String>(values));
    }

    /**
     * Sets the value of a xml parameter. Typically only invoked when reading the datacontrol definition from
     * DataControls.dcx and initializing the datacontrol providers.
     * @param name name of the parameter to set
     * @param xml new value for the parameter
     */
    @Override
    public void setXmlParameter(String name, org.w3c.dom.Node xml) {
        xmlParams.put(name, xml.cloneNode(true));
    }

    /**
     * Get a string parameter, or a globally set default value, or the specified default value but
     * without replacing any #{...} expressions.
     * @param name name of the parameter
     * @param namespace Class asking for this parameter which is used to retrieve the global parameter if no explicit
     * value has been set for this parameter for this provider. When invoking this, this should not be this.getClass(),
     * but the actual class asking for the parameter like MyProvider.class. This ensures that any subclass of MyProvider
     * still gets the global parameter for MyProvider.class and not MySubClassProvider.class if it would simply look
     * at this.getClass()
     * @param defaultValue default value to return when this parameter is not explicity set on this provider and no
     * global parameter could be found within the given namespace. Could be <tt>null</tt>.
     * @return explicit parameter for this provider, or otherwise the global parameter within the given namespace, or
     * otherwise the supplied default value
     * @see Configuration#getParameter
     * @see #setStringParameter
     */
    public String getRawStringParameter(final String name, final Class<? extends Provider> namespace,
                                        final String defaultValue) {
        String str;
        str = stringParams.get(name);
        if (str == null && namespace != null) { // no explicit value set, try global value from Configuration
            str = Configuration.getConfiguration().getParameter(qualifiedName(namespace, name));
        }
        if (str == null) { // if nothing worked, return given default
            str = defaultValue;
        }
        return str;
    }


    /**
     * invokes getRawStringParameter(String, Class, String) without a namespace and default, so returns <tt>null</tt>
     * when no explicit value has been set on this provider for given parameter name.
     * @see #getRawStringParameter(String,Class,String)
     * @deprecated superseded by getRawStringParameter(String, Class, String) with support for global default
     */
    @Deprecated
    @SuppressWarnings("oracle.jdeveloper.java.tag-is-missing")
    public String getRawStringParameter(final String name) {
        return getRawStringParameter(name, null, null);
    }

    /**
     * Get a list parameter, or a globally set default value, or the specified default value but
     * without replacing any #{...} expressions. When using a globally set default this is a string
     * which we split by comma and use each element as an entry in this list after trimming any whitespace
     * for each entry.
     * @param name name of the parameter
     * @param namespace Class asking for this parameter which is used to retrieve the global parameter if no explicit
     * value has been set for this parameter for this provider. When invoking this, this should not be this.getClass(),
     * but the actual class asking for the parameter like MyProvider.class. This ensures that any subclass of MyProvider
     * still gets the global parameter for MyProvider.class and not MySubClassProvider.class if it would simply look
     * at this.getClass()
     * @param defaultValue default value to return when this parameter is not explicity set on this provider and no
     * global parameter could be found within the given namespace. Could be <tt>null</tt>.
     * @return explicit parameter for this provider, or otherwise the global parameter within the given namespace, or
     * otherwise the supplied default value which could be <tt>null</tt>
     * @see Configuration#getParameter
     * @see #setListParameter
     */
    public List<String> getRawListParameter(final String name, final Class<? extends Provider> namespace,
                                            final List<String> defaultValue) {
        List<String> list = listParams.get(name);
        if (list == null && namespace != null) { // no explicit value set, try global value from Configuration
            final String s = Configuration.getConfiguration().getParameter(qualifiedName(namespace, name));
            if (s != null) {
                list = Arrays.asList(s.split(","));
            }
        }
        if (list == null) { // if nothing worked, return given default
            list = defaultValue;
        }
        return list == null ? null : Collections.unmodifiableList(list);
    }

    /**
     * invokes getRawListParameter(String, Class, List) without a namespace and default, so returns <tt>null</tt>
     * when no explicit value has been set on this provider for given parameter name.
     * @see #getRawListParameter(String,Class,List)
     * @deprecated superseded by getRawListParameter(String, Class, List) with support for global default
     */
    @Deprecated
    @SuppressWarnings("oracle.jdeveloper.java.tag-is-missing")
    public List<String> getRawListParameter(final String name) {
        return getRawListParameter(name, null, null);
    }

    /**
     * Get a xml parameter, or a globally set default value, or the specified default value but
     * without replacing any #{...} expressions. When using a globally set default this is a string
     * which we parse as XML and then use the document element.
     * @param name name of the parameter
     * @param namespace Class asking for this parameter which is used to retrieve the global parameter if no explicit
     * value has been set for this parameter for this provider. When invoking this, this should not be this.getClass(),
     * but the actual class asking for the parameter like MyProvider.class. This ensures that any subclass of MyProvider
     * still gets the global parameter for MyProvider.class and not MySubClassProvider.class if it would simply look
     * at this.getClass()
     * @param defaultValue default value to return when this parameter is not explicity set on this provider and no
     * global parameter could be found within the given namespace. Could be <tt>null</tt>.
     * @return explicit parameter for this provider, or otherwise the global parameter within the given namespace, or
     * otherwise the supplied default value while replacing any #{param...} expression with the actual values of that
     * dynamic paarameter from the given DataRequest
     * @see Configuration#getParameter
     * @see #setXmlParameter
     */
    public org.w3c.dom.Node getRawXmlParameter(String name, final Class<? extends Provider> namespace,
                                               final Node defaultValue) {
        Node node = xmlParams.get(name);
        if (node == null && namespace != null) { // no explicit value set, try global value from Configuration
            final String s = Configuration.getConfiguration().getParameter(qualifiedName(namespace, name));
            if (s != null) {
                try {
                    node = XmlParseUtils.parse(s).getDocumentElement();
                } catch (SAXException e) {
                    logger.severe("error parsing global xml parameter " + qualifiedName(namespace, name) + ":\n" + s);
                    throw new AdapterException(e);
                }
            }
        }
        if (node == null) { // if nothing worked, return given default
            node = defaultValue;
        }
        if (node == null) {
            return null;
        }
        return node.cloneNode(true); // use a clone before we start replacing dynamic param values or other manipulation
    }

    /**
     * invokes getRawXmlParameter(String, Class, Node) without a namespace and default, so returns <tt>null</tt>
     * when no explicit value has been set on this provider for given parameter name.
     * @see #getRawXmlParameter(String,Class,Node)
     * @deprecated superseded by getRawXmlParameter(String, Class, Node) with support for global default
     */
    @Deprecated
    @SuppressWarnings("oracle.jdeveloper.java.tag-is-missing")
    public org.w3c.dom.Node getRawXmlParameter(String name) {
        return getRawXmlParameter(name, null, null);
    }

    /**
     * Gets a string parameter, or possibly a global parameter or given default, while replacing all #{param...}
     * expressions with the values of the actual dynamic parameters.
     * @param name name of the parameter to retrieve
     * @param request DataRequest containing any dynamic parameter values that might be needed to replace #{param...}
     * epxressions
     * @param namespace Class asking for this parameter which is used to retrieve the global parameter if no explicit
     * value has been set for this parameter for this provider. When invoking this, this should not be this.getClass(),
     * but the actual class asking for the parameter like MyProvider.class. This ensures that any subclass of MyProvider
     * still gets the global parameter for MyProvider.class and not MySubClassProvider.class if it would simply look
     * at this.getClass()
     * @param defaultValue default value to return when this parameter is not explicity set on this provider and no
     * global parameter could be found within the given namespace. Could be {@code null}.
     * @return explicit parameter for this provider, or otherwise the global parameter within the given namespace, or
     * otherwise the supplied default value while replacing any #{param...} expression with the actual values of that
     * dynamic paarameter from the given DataRequest
     * @see Configuration#getParameter
     */
    public String getStringParameter(final String name, final DataRequest request,
                                     final Class<? extends Provider> namespace, final String defaultValue) {
        return replaceParameters(getRawStringParameter(name, namespace, defaultValue), request);
    }

    /**
     * Gets a list (of strings) parameter, or possibly a global parameter or given default, while replacing all
     * #{param...} expressions with the values of the actual dynamic parameters.
     * @param name name of the parameter to retrieve
     * @param request DataRequest containing any dynamic parameter values that might be needed to replace #{param...}
     * epxressions
     * @param namespace Class asking for this parameter which is used to retrieve the global parameter if no explicit
     * value has been set for this parameter for this provider. When invoking this, this should not be this.getClass(),
     * but the actual class asking for the parameter like MyProvider.class. This ensures that any subclass of MyProvider
     * still gets the global parameter for MyProvider.class and not MySubClassProvider.class if it would simply look
     * at this.getClass()
     * @param defaultValue default value to return when this parameter is not explicity set on this provider and no
     * global parameter could be found within the given namespace. Could be {@code null}.
     * @return explicit parameter for this provider, or otherwise the global parameter within the given namespace, or
     * otherwise the supplied default value while replacing any #{param...} expression with the actual values of that
     * dynamic paarameter from the given DataRequest
     * @see Configuration#getParameter
     */
    public List<String> getListParameter(String name, final DataRequest request,
                                         final Class<? extends Provider> namespace, final List<String> defaultValue) {
        final List<String> list = getRawListParameter(name, namespace, defaultValue);
        if (list == null) {
            return Collections.emptyList();
        }
        // replace all #{param...} before returning list
        final List<String> retval = new ArrayList<String>(list.size());
        for (String s : list) {
            retval.add(replaceParameters(s.trim(), request));
        }
        return retval;
    }

    /**
     * Gets a xml node parameter, or possibly a global parameter or given default, while replacing all #{param...}
     * expressions with the values of the actual dynamic parameters.
     * @param name name of the parameter to retrieve
     * @param request DataRequest containing any dynamic parameter values that might be needed to replace #{param...}
     * epxressions
     * @param namespace Class asking for this parameter which is used to retrieve the global parameter if no explicit
     * value has been set for this parameter for this provider. When invoking this, this should not be this.getClass(),
     * but the actual class asking for the parameter like MyProvider.class. This ensures that any subclass of MyProvider
     * still gets the global parameter for MyProvider.class and not MySubClassProvider.class if it would simply look
     * at this.getClass()
     * @param defaultValue default value to return when this parameter is not explicity set on this provider and no
     * global parameter could be found within the given namespace. Could be {@code null}.
     * @return explicit parameter for this provider, or otherwise the global parameter within the given namespace, or
     * otherwise the supplied default value while replacing any #{param...} expression with the actual values of that
     * dynamic paarameter from the given DataRequest
     * @see Configuration#getParameter
     */
    public Node getXmlParameter(String name, final DataRequest request, final Class<? extends Provider> namespace,
                                final Node defaultValue) {
        Node node = getRawXmlParameter(name, namespace, defaultValue);
        if (node == null) {
            return null;
        }
        replaceParameters(node, request);
        return node;
    }

    private String qualifiedName(final Class<? extends Provider> namespace, final String name) {
        return namespace.getName() + "." + name;
    }

    /**
     * invokes getStringParameter(String, DataRequest, Class, List) without a namespace and default, so returns <tt>null</tt>
     * when no explicit value has been set on this provider for given parameter name.
     * @see #getStringParameter(String,DataRequest,Class,String)
     * @deprecated see getStringParameter(String, DataRequest, Class, String)
     */
    @Deprecated
    @SuppressWarnings("oracle.jdeveloper.java.tag-is-missing")
    public String getStringParameter(final String name, final DataRequest request) {
        return replaceParameters(stringParams.get(name), request);
    }

    /**
     * invokes getListParameter(String, DataRequest, Class, List) without a namespace and default, so returns <tt>null</tt>
     * when no explicit value has been set on this provider for given parameter name.
     * @see #getListParameter(String,DataRequest,Class,List)
     * @deprecated see getListParameter(String, DataRequest, Class, List)
     */
    @Deprecated
    @SuppressWarnings("oracle.jdeveloper.java.tag-is-missing")
    public List<String> getListParameter(String name, final DataRequest request) {
        List<String> list = listParams.get(name);
        if (list == null) {
            return Collections.emptyList();
        }
        List<String> result = new ArrayList<String>(list.size());
        for (String s : list) {
            result.add(replaceParameters(s, request));
        }
        return result;
    }

    /**
     * invokes getXmlParameter(String, DataRequest, Class, Node) without a namespace and default, so returns <tt>null</tt>
     * when no explicit value has been set on this provider for given parameter name.
     * @see #getXmlParameter(String,DataRequest,Class,Node)
     * @deprecated see getXmlParameter(String, DataRequest, Class, Node)
     */
    @Deprecated
    @SuppressWarnings("oracle.jdeveloper.java.tag-is-missing")
    public Node getXmlParameter(String name, final DataRequest request) {
        Node node = xmlParams.get(name);
        if (node == null) {
            return null;
        }
        node = node.cloneNode(true); // use a clone before we start replacing dynamic param values
        replaceParameters(node, request);
        return node;
    }

    /**
     * Replace any #{param.xxx} expression in given string with the actual value of that
     * dynamic parameter.
     * @param string string which might contain zero, 1 or more #{param.xxx} expressions
     * @param request DataRequest with the actual values of the dynamic parameters
     * @return {@code string} with any {@code #{param.xxx}} expression replaced
     *         with the value of that dynamic parameters as defined in {@code request}
     */
    protected String replaceParameters(final String string, final DataRequest request) {
        if (string == null || request == null) {
            return string;
        }
        Map<String, Object> dynamicParamValues = request.getDynamicParamValues();
        TypeMapper typeMapper = request.getTypeMapper();
        if (dynamicParamValues == null || dynamicParamValues.isEmpty() || typeMapper == null) {
            return string;
        }
        String result = string;
        Matcher matcher = REPLACE_PATTERN.matcher(string);
        while (matcher.matches()) {
            String paramName = matcher.group(GROUP_PARAM_NAME);
            DynamicParameter dynamicParam = findDynamicParameter(paramName, request);
            if (dynamicParam.isXml()) {
                throw new IllegalArgumentException("dynamic-parameters of XML type are only supported in xml-parameters");
            }
            result =
                matcher.group(GROUP_BEFORE_PARAM) + getDynamicParameterXmlString(dynamicParam, request) +
                matcher.group(GROUP_AFTER_PARAM);
            matcher = REPLACE_PATTERN.matcher(result);
        }
        return result;
    }

    /**
     * Replace any #{param.xxx} expressions with their actual value in all TextNode
     * descendents of the given Node aswell as elements attributes.
     * <p>
     * Any Text node that contains a #{param.xxx} will be split into three separate
     * Text nodes: the text before the expression, the value of the expression itself
     * and the text after the expression.
     * <p>
     * This method will invoke itself recursively for all children of the given node so
     * all descendents are processed.
     * @param node the Node to start searching.
     * @param request The DataRequest with information about the current request, such
     *        as the dynamic parameter values.
     */
    protected void replaceParameters(final Node node, final DataRequest request) {
        if (node instanceof Text) {
            // try to find/replace dynamic parameters in text nodes
            String text = node.getNodeValue();
            Matcher matcher = REPLACE_PATTERN.matcher(text);
            if (matcher.matches()) {
                Document document = node.getOwnerDocument();
                Node parent = node.getParentNode();

                String prefix = matcher.group(GROUP_BEFORE_PARAM);
                String paramName = matcher.group(GROUP_PARAM_NAME);
                String postfix = matcher.group(GROUP_AFTER_PARAM);
                if (!prefix.isEmpty()) {
                    // simply keep text before dynamic parameter as new text node without any changes
                    Text prefixNode = document.createTextNode(prefix);
                    parent.insertBefore(prefixNode, node);
                }
                // replace #{param.xxx} with actual value
                DynamicParameter dynamicParam = findDynamicParameter(paramName, request);
                if (dynamicParam.isXml()) {
                    Object dynParVal = request.getDynamicParamValues().get(paramName);
                    if (dynParVal instanceof Node) {
                        // dynamic parameter is org.w3c.dom.Node (likely Element), replace #{param.xxx} with this Node
                        Node importedNode = document.importNode((Node) dynParVal, true); // import clones the source node
                        parent.insertBefore(importedNode, node);
                    } else if (dynParVal instanceof NodeList) {
                        // dynamic parameter is org.w3c.dom.NodeList (likely set of Elements)
                        // replace #{param.xxx} with this list of Nodes
                        NodeList nl = (NodeList) dynParVal;
                        for (int i = 0, num = nl.getLength(); i < num; i++) {
                            Node importedNode = document.importNode(nl.item(i), true);
                            parent.insertBefore(importedNode, node);
                        }
                    } else if (dynParVal !=
                               null) {
                        // ignore null values, anyhting else is unsupported
                        throw new IllegalArgumentException("dynamic parameter " + paramName + " of type " +
                                                           dynParVal.getClass() +
                                                           " is not a supported XML node or nodelist");
                    }
                } else {
                    // treat as simple type.
                    String dynParXml = getDynamicParameterXmlString(dynamicParam, request);
                    if (dynParXml != null && !dynParXml.isEmpty()) {
                        Text valueNode = document.createTextNode(dynParXml);
                        parent.insertBefore(valueNode, node);
                    }
                }
                // and keep text after dynamic parameter as text-node. Call replaceParameters recursively in case
                // this text contains another #{param.xxx}
                if (!postfix.isEmpty()) {
                    Text postfixNode = document.createTextNode(postfix);
                    parent.insertBefore(postfixNode, node);
                    replaceParameters(postfixNode, request);
                }
                // remove the node we just split up.
                parent.removeChild(node);
            }
        } else if (node.getNodeType() == Element.ELEMENT_NODE) {
            final Element element = (Element) node;
            final NamedNodeMap namedNodeMap = element.getAttributes();
            if(namedNodeMap != null){
                for(int i = 0, n=namedNodeMap.getLength(); i<n; i++){
                    final Node attr = namedNodeMap.item(i);
                    final String currentAttrValue = attr.getNodeValue();
                    final String tempAttr = replaceParameters(attr.getNodeValue(), request);
                    if(!StringUtils.equals(currentAttrValue, tempAttr)) {
                        element.setAttribute(attr.getNodeName(), tempAttr);
                    }
                }
            }
        }
        // traverse all children
        NodeList childNodes = node.getChildNodes();
        if (childNodes == null) {
            return;
        }
        for (int i = 0; i < childNodes.getLength(); i++) { // recursive call for all children
            replaceParameters(childNodes.item(i), request);
        }
    }

    /**
     * Returns the value of a dynamic parameter as a string that is safe to be used in XML.
     * @param dynamicParam DynamicParameter to retrieve the value for
     * @param request DataRequest containing all dynamic parameter values as well as the
     *                TypeMapper that should be used to convert the dynamic parameter value
     *                to a XML string.
     * @return value of the dynamic parameter as a XML string, which means it was converted
     *         by the TypeMapper from the DataRequest which is especially important for things
     *         like dates, times and byte-arrays which cannot use a simple toString().
     */
    private String getDynamicParameterXmlString(final DynamicParameter dynamicParam, final DataRequest request) {
        // get dynamic param value and typemapper
        TypeMapper typeMapper = request.getTypeMapper();
        Object dynaParamValue = request.getDynamicParamValues().get(dynamicParam.getName());
        // convert value to XML presentation
        String xmlValue = typeMapper.toXml(dynaParamValue, dynamicParam.getXmlType());
        return xmlValue;
    }

    /**
     * Find the definition of a given dynamic parameter so we now what java and xml type a dynamic parameter
     * is. This is normally retrieved from the given DataRequest but subclasses are free to override this
     * method to also return dynamic parameter definitions that are hardwired into the provider and not
     * exposed on the design time datacontrol as parameters.
     * @param paramName name of the dynamic parameter to locate
     * @param request DataRequest with all dynamic parameters that are defined in the DataControls.dcx for
     * this dataconol.
     * @return DynamicParameter definition, but never <tt>null</tt>
     */
    protected DynamicParameter findDynamicParameter(final String paramName, final DataRequest request) {
        Set<DynamicParameter> params = request.getDynamicParams();
        if (params == null) {
            return null;
        }
        for (DynamicParameter dp : params) {
            if (paramName.equals(dp.getName())) {
                return dp;
            }
        }
        logger.warning("unable to find definition for dynamic parameter {0}", paramName);
        return null;
    }
}
