package org.adfemg.datacontrol.xml.provider.filter;

import org.adfemg.datacontrol.xml.DataControl;
import org.adfemg.datacontrol.xml.provider.ProviderImpl;
import org.adfemg.datacontrol.xml.provider.data.DataProvider;
import org.adfemg.datacontrol.xml.provider.data.DataRequest;

import org.w3c.dom.Element;

/**
 * Special type of DataProvider that wraps another DataProvider. Can be used to alter the
 * supplied DataRequest before supplying to the wrapped DataProvider or alter th answer
 * from the wrapped DataProvider before returning it to the caller.
 * <p>
 * Can be configured in the DataControls.dcx with a nested DataProvider, which could be
 * another DataFilter. This can be used to chain multiple DataFilters that in the end should
 * contain a single plain DataProvider:
 * <p>
 * <pre>
 * &lt;data-provider class="com.example.OuterFilter">
 *   &lt;parameter name="param" value="parameter for OuterFilter"/>
 *   &lt;data-provider class="com.example.InnerFilter">
 *     &lt;parameter name="param" value="parameter for OuterFilter"/>
 *     &lt;data-provider class="org.adfemg.datacontrol.xml.provider.data.ResourceDataProvider">
 *       &lt;parameter name="resource" value="com/example/static.xml"/>
 *     &lt;/data-provider>
 *   &lt;/data-provider>
 * &lt;/data-provider>
 * </pre>
 * <p>
 * Each DataFilter (and DataProvider) can declare dynamic-parameters in the DataControls.dcx. Be
 * aware that the DataControl collects all dynamic parameters for all the DataFilter and DataProviders
 * and stores them in a single Map when supplying it to the filters and providers through the
 * DataRequest. This means that the name of all dynamic parameters have to be unique across filters.
 * The upside of this approach is that each DataFilter can manipulate the values of the dynamic
 * parameters before they are passed to the nested DataProvuder(s).
 */
public abstract class DataFilter extends ProviderImpl implements DataProvider {

    final DataProvider source;

    public DataFilter(final DataProvider source) {
        this.source = source;
    }

    /**
     * Returns the Element from the source DataProvider that was supplied to the
     * constructor. Can be used by concrete subclasses when implementing getRootElement
     * @param request DataRequest
     * @return the Element as returned by the source DataProvider, or {@code null}
     *         if no source DataProvider is known
     */
    protected Element getSourceElement(DataRequest request) {
        return source == null ? null : source.getRootElement(request);
    }

    @Override
    public void dataControlCreated(DataControl dc) {
        super.dataControlCreated(dc);
        // also let nested DataProvider know our datacontrol has been created
        if (source != null) {
            source.dataControlCreated(dc);
        }
    }

}
