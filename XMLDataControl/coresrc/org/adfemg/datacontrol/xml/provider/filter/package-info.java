/**
 * Filter Data Providers are special types of Data Providers that nest other Data Providers and process the results
 * of these nested providers before returning them to the upstream Filter Data Provider or XML DataControl itself.
 */
package org.adfemg.datacontrol.xml.provider.filter;

