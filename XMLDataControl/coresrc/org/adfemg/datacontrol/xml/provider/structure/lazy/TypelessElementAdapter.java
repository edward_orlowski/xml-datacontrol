package org.adfemg.datacontrol.xml.provider.structure.lazy;

import oracle.binding.meta.NamedDefinition;

import org.adfemg.datacontrol.xml.provider.typemap.TypeMapper;

public class TypelessElementAdapter extends AbstractAttributeDefinition {

    public TypelessElementAdapter(NamedDefinition parent, String simpleName, TypeMapper typeMapper) {
        super(parent, simpleName, typeMapper);
    }

    @Override
    public String getJavaTypeString() {
        return getTypeMapper().getJavaType(null);
    }

}
