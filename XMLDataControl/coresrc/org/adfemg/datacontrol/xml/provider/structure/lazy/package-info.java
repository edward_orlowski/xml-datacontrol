/**
 * Supporting classes for the Lazy Structure Provider that doesn't process an entire XML Schema to datacontrol
 * structures at once, but returns datacontrol structures that wrap XML Schema elements and won't process
 * its nested structures until asked to.
 */
package org.adfemg.datacontrol.xml.provider.structure.lazy;

