package org.adfemg.datacontrol.xml.provider.structure;

import oracle.binding.meta.StructureDefinition;

/**
 * Sub-interface of StructureDefinition that adds a single method that allows changing the
 * parent of the StructureDefinition after creation. This is required by the StructureProvider
 * that needs to return a StructureDefinition that can be linked to a MethodReturn in
 * the datacontrol.
 */
public interface MovableStructureDefinition extends StructureDefinition, MovableNamedDefinition {
}
