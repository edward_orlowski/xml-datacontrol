/**
 * Structure Providers determine the data structure of the XML DataControl and all its nested data structures. This is
 * typically done by processing an XML Schema document, but alternative Structure Providers can also be used.
 */
package org.adfemg.datacontrol.xml.provider.structure;

