package org.adfemg.datacontrol.xml.provider.structure.lazy;

import oracle.binding.meta.NamedDefinition;

import org.adfemg.datacontrol.xml.provider.typemap.TypeMapper;

public class AnyTypeElementAdapter extends AbstractAttributeDefinition {

    public AnyTypeElementAdapter(NamedDefinition parent, String simpleName, TypeMapper typeMapper) {
        super(parent, simpleName, typeMapper);
    }

    @Override
    public String getJavaTypeString() {
        return "org.w3c.dom.Element";
    }

}
