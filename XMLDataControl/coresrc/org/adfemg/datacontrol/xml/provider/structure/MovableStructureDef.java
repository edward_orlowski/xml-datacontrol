package org.adfemg.datacontrol.xml.provider.structure;

import oracle.adf.model.adapter.dataformat.StructureDef;

import oracle.binding.meta.NamedDefinition;

/**
 * Empty subclass of StructureDef that simply adds the MovableStructureDefintion interface.
 * StructureDef already has the method required by MovableStructureDefinition but does not declare this
 * in its interface. StructureProviders can simply use this MovableStructureDef instead of
 * plain StructureDef and comply with the interface of a StructureProvider that needs to return a
 * MovableStructureDefinition.
 * Having this MovableStructureDefinition sub-interface of StructureDefinition allows StructureProviders
 * to supply their own implementation of this interface without relying on the default StructureDef
 * implementation class.
 */
public class MovableStructureDef extends StructureDef implements MovableStructureDefinition {

    public MovableStructureDef(String fullName, NamedDefinition parent) {
        super(fullName, parent);
    }

    public MovableStructureDef(String fullName) {
        super(fullName);
    }

}
