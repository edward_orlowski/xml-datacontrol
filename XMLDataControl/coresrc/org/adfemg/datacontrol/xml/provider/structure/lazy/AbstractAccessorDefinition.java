package org.adfemg.datacontrol.xml.provider.structure.lazy;

import oracle.binding.meta.AccessorDefinition;
import oracle.binding.meta.Definition;
import oracle.binding.meta.NamedDefinition;
import oracle.binding.meta.StructureDefinition;

import org.adfemg.datacontrol.xml.provider.typemap.TypeMapper;

public abstract class AbstractAccessorDefinition extends AbstractAttributeDefinition implements AccessorDefinition {

    private StructureDefinition operations;

    public AbstractAccessorDefinition(NamedDefinition parent, String simpleName, TypeMapper typeMapper) {
        super(parent, simpleName, typeMapper);
    }

    @Override
    public int getDefinitionType() {
        return Definition.TYPE_ACCESSOR;
    }

    @Override
    public boolean isScalarCollection() {
        return false;
    }

    @Override
    public StructureDefinition getCollectionStructure() {
        if (operations == null) {
            operations =
                isCollection() ? new CollectionOperations(this, getTypeMapper()) :
                new ScalarOperations(this, getTypeMapper());
        }
        return operations;
    }

    @Override
    public AccessorDefinition getParentAccessor() {
        throw new UnsupportedOperationException("getParentAccessor");
    }

    @Override
    public String getJavaTypeString() {
        return null;
    }

}
