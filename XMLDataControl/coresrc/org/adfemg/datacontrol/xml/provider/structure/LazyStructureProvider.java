package org.adfemg.datacontrol.xml.provider.structure;

import oracle.xml.parser.schema.XSDComplexType;
import oracle.xml.parser.schema.XSDElement;
import oracle.xml.parser.schema.XSDNode;

import org.adfemg.datacontrol.xml.provider.ProviderImpl;
import org.adfemg.datacontrol.xml.provider.structure.lazy.ComplexTypeAdapter;
import org.adfemg.datacontrol.xml.provider.typemap.TypeMapper;

public class LazyStructureProvider extends ProviderImpl implements StructureProvider {

    @Override
    public MovableStructureDefinition buildStructure(XSDNode node, String structName, TypeMapper typeMapper) {
        XSDComplexType type = (XSDComplexType) (node instanceof XSDComplexType ? node : ((XSDElement) node).getType());
        return new ComplexTypeAdapter(null, structName, typeMapper, type);
    }

}
