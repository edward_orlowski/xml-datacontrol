package org.adfemg.datacontrol.xml.provider;

import javax.xml.namespace.QName;

import org.adfemg.datacontrol.xml.utils.ClassUtils;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DynamicParameter {

    private String name;
    // we don't store actual java.lang.Class instances as the class might only be available
    // at runtime and not at design-time. Want to prevent classloader errors at design time when
    // compiled class doesn't exist (yet)
    private String javaType;
    private QName xmlType;

    public DynamicParameter(final String name, final String javaType, final QName xmlType) {
        this.name = name;
        this.javaType = javaType;
        this.xmlType = xmlType;
    }

    public String getName() {
        return name;
    }

    public String getJavaType() {
        return javaType;
    }

    public QName getXmlType() {
        return xmlType;
    }

    public boolean isXml() {
        if (javaType.endsWith("[]")) {
            return false; // prevent error with loadClass with byte[]
        }
        Class<?> cls = ClassUtils.loadClass(javaType);
        return (Node.class.isAssignableFrom(cls) || NodeList.class.isAssignableFrom(cls));
    }

}
