package org.adfemg.datacontrol.xml.provider.data.ws;

import java.util.List;

import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.HandlerResolver;
import javax.xml.ws.handler.PortInfo;

import org.adfemg.datacontrol.xml.provider.data.DataRequest;
import org.adfemg.datacontrol.xml.provider.data.WSDataProvider;

/**
 * Implementation of HandlerResolver that gets the actual list of Handlers to return from a given WSDataProvider
 * for a given DataRequest so the WSDataProvider has control over the list of Handlers.
 */
public class HandlerResolverImpl implements HandlerResolver {

    private WSDataProvider dataProvider;
    private DataRequest request;

    /**
     * Default no-arg constructor. Be sure to call setDataProvider and setRequest before handing this
     * HandlerResolver of to the JAX-WS framework that will be invoking getHandlerChain.
     */
    public HandlerResolverImpl() {
    }

    /**
     * Sets the WSDataProvider that can build the list of Handlers to return.
     * @param provider WSDataProvider
     */
    public void setDataProvider(final WSDataProvider provider) {
        this.dataProvider = provider;
    }

    /**
     * Gets the WSDataProvider this HandlerResolver is for.
     * @return WSDataProvider
     */
    protected WSDataProvider getDataProvider() {
        return dataProvider;
    }

    /**
     * Sets the DataRequest that needs to be given to WSDataProvider.getHandlers to build a list
     * of Handlers for this request.
     * @param request DataRequest this HandlerResolver should get the Handlers for
     * @see WSDataProvider#getHandlers
     */
    public void setRequest(DataRequest request) {
        this.request = request;
    }

    /**
     * Gets the DataRequest this HandlerResolver is for.
     * @return DataRequest
     */
    protected DataRequest getRequest() {
        return request;
    }

    /**
     * Gets the list of Handlers for this request by invoking WSDataProvider.getHandlers.
     * @param portInfo JAX-WS service port to return the handlers for
     * @return list of Handlers as returned by WSDataProvider.getHandlers
     * @see WSDataProvider#getHandlers
     */
    @Override
    public List<Handler> getHandlerChain(PortInfo portInfo) {
        return getDataProvider().getHandlers(getRequest());
    }

}
