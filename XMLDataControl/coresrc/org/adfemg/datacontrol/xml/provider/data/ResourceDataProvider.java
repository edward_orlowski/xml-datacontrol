package org.adfemg.datacontrol.xml.provider.data;

import java.net.URL;

import oracle.adf.model.adapter.AdapterException;

import org.adfemg.datacontrol.xml.provider.ProviderImpl;
import org.adfemg.datacontrol.xml.utils.URLUtils;
import org.adfemg.datacontrol.xml.utils.XmlFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


/**
 * De Resource DataProvider.
 *
 * //TODO: Describe ......
 *
 * @see ProviderImpl
 * @see DataProvider
 * @see org.adfemg.datacontrol.xml.DataControl
 */
public class ResourceDataProvider extends ProviderImpl implements DataProvider {

    public static final String PARAM_RESOURCE = "resource";

    /**
     * Default no-argument constructor.
     */
    public ResourceDataProvider() {
        super();
    }

    @Override
    public Element getRootElement(final DataRequest request) {
        String resource = getStringParameter(PARAM_RESOURCE, request, ResourceDataProvider.class, null);
        if (resource == null) {
            throw new IllegalArgumentException("mandatory parameter " + PARAM_RESOURCE + " is unknown");
        }
        URL url = URLUtils.findResource(resource);
        //InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream(resource);
        if (url == null) {
            throw new IllegalArgumentException("resource \"" + resource + "\" not found");
        }
        try {
            Document doc = XmlFactory.newDocumentBuilder().parse(url.toExternalForm());
            return doc.getDocumentElement();
        } catch (Exception e) {
            throw new AdapterException(e);
        }
    }
}
