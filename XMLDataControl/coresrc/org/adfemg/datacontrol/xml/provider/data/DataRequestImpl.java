package org.adfemg.datacontrol.xml.provider.data;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

import oracle.binding.meta.StructureDefinition;

import oracle.xml.parser.schema.XMLSchema;
import oracle.xml.parser.schema.XSDException;

import org.adfemg.datacontrol.xml.DataControlDefinitionNode;
import org.adfemg.datacontrol.xml.provider.DynamicParameter;
import org.adfemg.datacontrol.xml.provider.typemap.TypeMapper;

public class DataRequestImpl implements DataRequest {

    private StructureDefinition structureDefinition;
    private Map<String, Object> dynamicParamValues;
    private DataControlDefinitionNode defNode;

    public DataRequestImpl(final StructureDefinition structureDefinition, final Map<String, Object> dynamicParamValues,
                           final DataControlDefinitionNode defNode) {
        this.structureDefinition = structureDefinition;
        this.dynamicParamValues = Collections.unmodifiableMap(dynamicParamValues);
        this.defNode = defNode;
    }

    @Override
    public StructureDefinition getStructureDefinition() {
        return structureDefinition;
    }

    /**
     * Returns unmodifiable map of dynamic parameter values as provided by the DataControl (from
     * the binding layer) or by an upstream DataFilter. When chaining DataFilters do not modify the
     * values in this map but create a new map with parameters and clone/duplicate the object you
     * want to change before passing it to the nested DataProvider. The reason behind this is
     * that the DataControl will use the content of this map to determine whether two requests
     * are the same and a cached result can be returned after a failover in a high-availability
     * cluster. If the objects in the map itself would be modified the map would not be equal to the
     * "same" subsequent request.
     * @return unmodifiable map of dynamic parameters
     */
    @Override
    public Map<String, Object> getDynamicParamValues() {
        return dynamicParamValues;
    }

    @Override
    public Set<DynamicParameter> getDynamicParams() {
        return defNode.getDynamicParams();
    }

    @Override
    public TypeMapper getTypeMapper() {
        return defNode.getProviderInstance(TypeMapper.class);
    }

    @Override
    public XMLSchema getSchema() throws XSDException {
        return defNode.getSchema();
    }

    @Override
    public String getRootNodeName() {
        return defNode.getRoot();
    }

}
