package org.adfemg.datacontrol.xml.provider.data;


import org.adfemg.common.events.MutableObject;

import org.w3c.dom.Element;


/**
 * When a XML Element is wrapped by this interface, the Object will be notifified
 * about changes to the data or structure of the XML.
 *
 * @see MutableObject
 */
public interface MutableElementWrapper extends MutableObject {
    /**
     * Notify about the change on the XML Element.
     */
    void notifyChanged();

    /**
     * Getter for the Element.
     * @return The element.
     */
    Element getElement();
}
