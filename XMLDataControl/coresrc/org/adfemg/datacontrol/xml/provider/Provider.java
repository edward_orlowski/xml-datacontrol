package org.adfemg.datacontrol.xml.provider;

import java.util.List;

import org.adfemg.datacontrol.xml.DataControl;

/**
 * Provider interface.
 */
public interface Provider {

    // after create provider
    void setStringParameter(String name, String value);

    void setListParameter(String name, List<String> values);

    void setXmlParameter(String name, org.w3c.dom.Node xml);

    // after create dataControl
    void dataControlCreated(DataControl dc);
}
