/**
 * TypeMapper Providers determine which java type should be returned for a given XML type and also provides runtime
 * mapping between these xml and java types in both directions.
 */
package org.adfemg.datacontrol.xml.provider.typemap;

