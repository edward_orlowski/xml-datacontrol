package org.adfemg.datacontrol.xml.provider.typemap;

import javax.xml.namespace.QName;

import oracle.xml.parser.schema.XSDSimpleType;

import org.adfemg.datacontrol.xml.provider.Provider;


/**
 * The TypeMapper is used by the DataControlDefinition to determine what Java
 * type to use to represent an XML Schema datatype.
 *
 * The default implementation is {@link TypeMapperImpl}.
 *
 * @see org.adfemg.datacontrol.xml.DataControlDefinition
 * @see TypeMapperImpl
 */
public interface TypeMapper extends Provider {

    /**
     * Indicates if for a given SimpleType from the XSD this type can be mapped
     * with the TypeMapper or based on the super type of the SimpleType.
     *
     * <p>For example, it can be desirable to have an own SimpleType named
     * "MySimpleType", which is an extension of xsd:integer. Run time it will be
     * sufficient to map from an XML xsd:integer to Java integer and vice versa.
     * The mapper does not need to know about "MySimpleType".
     * In this example the {@code getMappableType(MySimpleType)} needs to
     * return {@code xsd:integer}.
     *
     * <p>An other example can be that it is desirable to have an own SimpleType
     * named "typeYesNo" mapped to boolean in Java, while the XSD has an
     * restriction on xsd:string. In this case the mapper needs to know that the
     * mapping does not occur from the xsd:string but from the typeYesNo.
     * In this example the {@code getMappableType(typeYesNo)} needs to
     * return {@code typeYesNo}.
     *
     * @param xsdType XSD SimpleType can be asked which XSD SimpleType will be used
     *                at runtime by the TypeMapper.
     * @return The Name as QName from the xsdType or of one of the base types
     *         of xsdType. This base types can be obtained by recursively
     *         calling {@link XSDSimpleType#getBase}.
     */
    QName getMappableType(XSDSimpleType xsdType);

    /**
     * Get the JavaType that is used in the DataControl to represent the
     * given SimpleType from the XSD.
     *
     * @param xsdType XSD SimpleType or {@code null} for elements without
     *                explicite type
     * @return The name of the Java class, for example: java.lang.String
     */
    String getJavaType(XSDSimpleType xsdType);

    /**
     * Maps from an XML Type to a Java Type.
     * Returns the new JavaType with the same value.
     *
     * @param xmlContent The XML Content.
     * @param xmlType The XML Type.
     * @param javaType The Java Type.
     * @return the new JavaType with the value from the xmlContent.
     */
    Object toJava(String xmlContent, QName xmlType, String javaType);

    /**
     * Map the Java Type to the XML type.
     *
     * @param javaValue The Java input type.
     * @param xmlType The QName XML Type.
     * @return the XML value as a String.
     */
    String toXml(Object javaValue, QName xmlType);

}
