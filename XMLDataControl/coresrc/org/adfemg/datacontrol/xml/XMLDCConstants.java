package org.adfemg.datacontrol.xml;

import java.util.HashMap;
import java.util.Map;

import javax.xml.namespace.QName;

import org.adfemg.datacontrol.xml.provider.Provider;
import org.adfemg.datacontrol.xml.provider.customization.CustomizationProvider;
import org.adfemg.datacontrol.xml.provider.customization.NullCustomizationProvider;
import org.adfemg.datacontrol.xml.provider.data.DataProvider;
import org.adfemg.datacontrol.xml.provider.data.ELDataProvider;
import org.adfemg.datacontrol.xml.provider.structure.SchemaStructureProvider;
import org.adfemg.datacontrol.xml.provider.structure.StructureProvider;
import org.adfemg.datacontrol.xml.provider.typemap.TypeMapper;
import org.adfemg.datacontrol.xml.provider.typemap.TypeMapperImpl;

/**
 * Class with Constants for the XML DataControl.
 */
public final class XMLDCConstants {

    public static final String NS_XMLSCHEMA = "http://www.w3.org/2001/XMLSchema";

    // see http://www.w3.org/TR/2004/PER-xmlschema-2-20040318/#built-in-datatypes for types
    public static final QName QNAME_SCHEMA_ANYTYPE = new QName(NS_XMLSCHEMA, "anyType");
    public static final QName QNAME_SCHEMA_ANYSIMPLETYPE = new QName(NS_XMLSCHEMA, "anySimpleType");
    public static final QName QNAME_SCHEMA_STRING = new QName(NS_XMLSCHEMA, "string");
    public static final QName QNAME_SCHEMA_NORMSTRING = new QName(NS_XMLSCHEMA, "normalizedString");
    public static final QName QNAME_SCHEMA_TOKEN = new QName(NS_XMLSCHEMA, "token");
    public static final QName QNAME_SCHEMA_LANGUAGE = new QName(NS_XMLSCHEMA, "language");
    public static final QName QNAME_SCHEMA_NAME = new QName(NS_XMLSCHEMA, "Name");
    public static final QName QNAME_SCHEMA_NCNAME = new QName(NS_XMLSCHEMA, "NCName");
    public static final QName QNAME_SCHEMA_ID = new QName(NS_XMLSCHEMA, "ID");
    public static final QName QNAME_SCHEMA_IDREF = new QName(NS_XMLSCHEMA, "IDREF");
    public static final QName QNAME_SCHEMA_IDREFS = new QName(NS_XMLSCHEMA, "IDREFS");
    public static final QName QNAME_SCHEMA_ENTITY = new QName(NS_XMLSCHEMA, "ENTITY");
    public static final QName QNAME_SCHEMA_ENTITIES = new QName(NS_XMLSCHEMA, "ENTITIES");
    public static final QName QNAME_SCHEMA_NMTOKEN = new QName(NS_XMLSCHEMA, "NMTOKEN");
    public static final QName QNAME_SCHEMA_NMTOKENS = new QName(NS_XMLSCHEMA, "NMTOKENS");
    public static final QName QNAME_SCHEMA_BOOLEAN = new QName(NS_XMLSCHEMA, "boolean");
    public static final QName QNAME_SCHEMA_BASE64BIN = new QName(NS_XMLSCHEMA, "base64Binary");
    public static final QName QNAME_SCHEMA_HEXBIN = new QName(NS_XMLSCHEMA, "hexBinary");
    public static final QName QNAME_SCHEMA_FLOAT = new QName(NS_XMLSCHEMA, "float");
    public static final QName QNAME_SCHEMA_DECIMAL = new QName(NS_XMLSCHEMA, "decimal");
    public static final QName QNAME_SCHEMA_INTEGER = new QName(NS_XMLSCHEMA, "integer");
    public static final QName QNAME_SCHEMA_NONPOSINTEGER = new QName(NS_XMLSCHEMA, "nonPositiveInteger");
    public static final QName QNAME_SCHEMA_NEGINTEGER = new QName(NS_XMLSCHEMA, "negativeInteger");
    public static final QName QNAME_SCHEMA_LONG = new QName(NS_XMLSCHEMA, "long");
    public static final QName QNAME_SCHEMA_INT = new QName(NS_XMLSCHEMA, "int");
    public static final QName QNAME_SCHEMA_SHORT = new QName(NS_XMLSCHEMA, "short");
    public static final QName QNAME_SCHEMA_BYTE = new QName(NS_XMLSCHEMA, "byte");
    public static final QName QNAME_SCHEMA_NONNEGINTEGER = new QName(NS_XMLSCHEMA, "nonNegativeInteger");
    public static final QName QNAME_SCHEMA_UNSLONG = new QName(NS_XMLSCHEMA, "unsignedLong");
    public static final QName QNAME_SCHEMA_POSINT = new QName(NS_XMLSCHEMA, "positiveInteger");
    public static final QName QNAME_SCHEMA_UNSINT = new QName(NS_XMLSCHEMA, "unsignedInt");
    public static final QName QNAME_SCHEMA_UNSSHORT = new QName(NS_XMLSCHEMA, "unsignedShort");
    public static final QName QNAME_SCHEMA_UNSBYTE = new QName(NS_XMLSCHEMA, "unsignedByte");
    public static final QName QNAME_SCHEMA_DOUBLE = new QName(NS_XMLSCHEMA, "double");
    public static final QName QNAME_SCHEMA_ANYURI = new QName(NS_XMLSCHEMA, "anyURI");
    public static final QName QNAME_SCHEMA_QNAME = new QName(NS_XMLSCHEMA, "QName");
    public static final QName QNAME_SCHEMA_NOTATION = new QName(NS_XMLSCHEMA, "NOTATION");
    public static final QName QNAME_SCHEMA_DURATION = new QName(NS_XMLSCHEMA, "duration");
    public static final QName QNAME_SCHEMA_DATETIME = new QName(NS_XMLSCHEMA, "dateTime");
    public static final QName QNAME_SCHEMA_TIME = new QName(NS_XMLSCHEMA, "time");
    public static final QName QNAME_SCHEMA_DATE = new QName(NS_XMLSCHEMA, "date");
    public static final QName QNAME_SCHEMA_GYEARMONTH = new QName(NS_XMLSCHEMA, "gYearMonth");
    public static final QName QNAME_SCHEMA_GYEAR = new QName(NS_XMLSCHEMA, "gYear");
    public static final QName QNAME_SCHEMA_GMONTHDAY = new QName(NS_XMLSCHEMA, "gMonthDay");
    public static final QName QNAME_SCHEMA_GDAY = new QName(NS_XMLSCHEMA, "gDay");
    public static final QName QNAME_SCHEMA_GMONTH = new QName(NS_XMLSCHEMA, "gMonth");

    public static final String CLSNAME_STRING = "java.lang.String";
    public static final String CLSNAME_BOOLEAN = "java.lang.Boolean";
    public static final String CLSNAME_BIGDECIMAL = "java.math.BigDecimal";
    public static final String CLSNAME_BIGINTEGER = "java.math.BigInteger";
    public static final String CLSNAME_LONG = "java.lang.Long";
    public static final String CLSNAME_INTEGER = "java.lang.Integer";
    public static final String CLSNAME_SHORT = "java.lang.Short";
    public static final String CLSNAME_BYTE = "java.lang.Byte";
    public static final String CLSNAME_UTILDATE = "java.util.Date";
    public static final String CLSNAME_SQLDATE = "java.sql.Date";
    public static final String CLSNAME_BYTEARRAY = "byte[]";
    public static final String CLSNAME_URI = "java.net.URI";
    public static final String CLSNAME_XMLQNAME = "javax.xml.namespace.QName";
    public static final String CLSNAME_XMLDURATION = "javax.xml.datatype.Duration";

    // key=interface of provider type, value=default class
    public static final Map<Class<? extends Provider>, Class<? extends Provider>> DFLT_PROVIDERS =
        new HashMap<Class<? extends Provider>, Class<? extends Provider>>() {
            {
                put(DataProvider.class, ELDataProvider.class);
                put(TypeMapper.class, TypeMapperImpl.class);
                put(CustomizationProvider.class, NullCustomizationProvider.class);
                put(StructureProvider.class, SchemaStructureProvider.class);
            }
        };

    private XMLDCConstants() { // non instantiable
    }

}
