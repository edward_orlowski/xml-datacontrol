package org.adfemg.datacontrol.xml.cust;

import java.lang.reflect.Method;

import java.util.Collection;
import java.util.Collections;

import oracle.adf.model.adapter.dataformat.StructureDef;

import org.adfemg.datacontrol.xml.annotation.Created;
import org.adfemg.datacontrol.xml.data.XMLDCElement;
import org.adfemg.datacontrol.xml.handler.CreatedHandler;
import org.adfemg.datacontrol.xml.handler.Handler;
import org.adfemg.datacontrol.xml.java.JAnnotation;
import org.adfemg.datacontrol.xml.java.JMethod;
import org.adfemg.datacontrol.xml.utils.ClassUtils;


/**
 * The Adporter for the Created annotation.
 *
 * @see Adopter
 * @see Created
 */
public class CreatedAdopter implements Adopter<Created> {

    /**
     * Check the method signature of the Created method.
     * The method should be equals to 'elementCreated'.
     *
     * @inheritDoc
     */
    @Override
    public void checkMethodSignature(JMethod method, JAnnotation annotation,
                                     StructureDef structure) throws InvalidMethodSignatureException {
        AdopterUtil.checkSignature(method, Void.class, XMLDCElement.class);
        AdopterUtil.checkMethodEquals(method, "elementCreated");
    }

    @Override
    public void adjustStructure(StructureDef structure, JAnnotation annotation, JMethod method) {
    }

    /**
     * Creates a CreatedHandler that fires the method created in the
     * customization class with the XMLDCElement as arguments.
     *
     * @see CreatedHandler
     * @see XMLDCElement
     * @inheritDoc
     */
    @Override
    public Collection<Handler> createHandlers(final Created annotation, final Method method, final Object customizer) {
        CreatedHandler handler = new CreatedHandler() {
            @Override
            public void invokeCreated(XMLDCElement element) {
                ClassUtils.invokeMethod(customizer, method, element);
            }
        };
        return Collections.<Handler>singletonList(handler);
    }
}
