package org.adfemg.datacontrol.xml.design.wizard;

import java.awt.Image;

import oracle.bali.ewt.dialog.JEWTDialog;

import oracle.ide.Context;
import oracle.ide.Ide;

import oracle.javatools.dialogs.ExceptionDialog;

import oracle.jdeveloper.wizard.common.BaliWizard;
import oracle.jdeveloper.wizard.common.BaliWizardPanel;
import oracle.jdeveloper.wizard.common.BaliWizardState;


public class GalleryWizard extends BaliWizard {

    @Override
    public boolean isAvailable(Context context) {
        // addin requires a project to be selected in JDev
        return (context != null) && (context.getProject() != null);
    }

    @Override
    public boolean invoke(Context context) {
        try {
            if (context.getProject() == null) {
                throw new IllegalStateException(GalleryWizard.class.getName() + " requires a selected project");
            }
            super.invoke(context);
        } catch (Exception e) {
            e.printStackTrace();
            ExceptionDialog.showExceptionDialog(Ide.getMainWindow(), e, "Application Error");
        }
        return true;
    }

    @Override
    protected JEWTDialog buildDialog(Context context, BaliWizardState state) {
        JEWTDialog dialog = super.buildDialog(context, state);
        dialog.setResizable(true);
        return dialog;
    }

    @Override
    protected BaliWizardPanel buildPanel(Context context, BaliWizardState baliWizardState) {
        GalleryWizardPanel panel = new GalleryWizardPanel();
        // HelpSystem.getHelpSystem().registerTopic(panel, panel.getHelpString());
        return panel;
    }

    @Override
    protected Image getHeaderImage() {
        //return OracleIcons.toImage(OracleIcons.getIcon("header/placeholderHeader.png"));
        return super.getHeaderImage();
    }

    @Override
    protected BaliWizardState buildState(Context context) {
        GalleryWizardState state = new GalleryWizardState(context);
        return state;
    }

    @Override
    protected String getHeaderDescription() {
        return "Provide the details of the new XML Data Control.";
    }

    @Override
    protected String getDialogTitle() {
        return "Create XML Data Control";
    }

    @Override
    public String getShortLabel() {
        return "XML Data Control";
    }

}
