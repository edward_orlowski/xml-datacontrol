package org.adfemg.datacontrol.xml.design;

import java.beans.Beans;

/**
 * Utility methods for determining if we are running in design time or run time mode and whether it is safe to
 * use some of the design time classes that need Oracle JDeveloper classes that won't be available at runtime.
 */
public final class DesignTimeUtils {

    /**
     * Private constructor to prevent instances being created.
     */
    private DesignTimeUtils() {
    }

    /**
     * Returns true if the current application is running in design time mode, which means the JDeveloper IDE
     * or any of its non-GUI helpers like ojaudit or ojdeploy. You can assume the JDeveloper IDE extension classes
     * to be available if this returns {@code true} but be aware that there might not be a graphical user interface
     * in command line tools like ojdeploy or ojaudit. If you want to use GUI design time elements like dialog boxes
     * be sure to check isGraphicalDesignTime()
     * @return {@code true} if the current application is the JDeveloper IDE or any of its command line tools like
     * ojaudit or ojdeploy.
     * @see #isGraphicalDesignTime
     */
    public static boolean isDesignTime() {
        return Beans.isDesignTime();
    }

    /**
     * Returns true if both isDesignTime and isGuiAvailable return true which means we are running in a graphical
     * design time environment, typically JDeveloper, and it is safe to use GUI elements like dialog boxes.
     * @return true if the current application has a graphical user interface and is running in design time mode
     */
    public static boolean isGraphicalDesignTime() {
        return isDesignTime() && isGuiAvailable();
    }

    /**
     * Returns {@code true} if a graphical user interface is available.
     * @return {@code true} if a GUI exists
     * @see #isGraphicalDesignTime
     */
    public static boolean isGuiAvailable() {
        return Beans.isGuiAvailable();
    }

}
