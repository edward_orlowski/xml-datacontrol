package org.adfemg.datacontrol.xml.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * Use this annotation to define a PostAttrChange method.
 * This method will be fired after the attribute has been changed. The
 * name of the annotated method determines which attribute has to
 * be changed for the method to be called. For example an annotated
 * method nameChanged will be called if the {@code name} attribute is
 * changed. If the attribute name contains illegal characters or starts with
 * an uppercase letter you can also use the attr attribute on the annotation
 * to force the name of the attribute, for example<br>
 * {@code @PostAttrChange(attr="Some*name") }
 * <p>
 * The signature of the annotated method has to be:<br/>
 * <code>public void <i>attrName</i>Changed(AttrChangeEvent event)</code>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
@Documented
public @interface PostAttrChange {
    String attr() default AnnotationHelper.DFLT_ATTR_STRING;
}
