package org.adfemg.datacontrol.xml.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Use this annotation to create a calculated attribute.
 * This attribute is a read-only attribuut, based on one or multiple other attributes.
 * <p>
 * The signature of the annotated method has to be:<br/>
 * <code>public <i>AttributeJavaClass</i> get<i>AttrName</i>(XMLDCElement department)</code>
 * <p>
 * The datatype and name of the calculated attribute are derived from
 * this method signature.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
@Documented
public @interface CalculatedAttr {
}
