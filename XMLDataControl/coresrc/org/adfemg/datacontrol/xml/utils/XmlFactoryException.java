package org.adfemg.datacontrol.xml.utils;


/**
 * Exception for very unusual cases where {@link XmlFactory} fails to initialize one of its factory classes.
 */
public class XmlFactoryException extends RuntimeException {

    @SuppressWarnings("compatibility:-7564759065411648682")
    private static final long serialVersionUID = 1L;

    /**
     * Constructs a new runtime exception with the specified detail message and cause.
     * @param message the detail message (which is saved for later retrieval by the Throwable.getMessage() method)
     * @param cause the cause (which is saved for later retrieval by the Throwable.getCause() method)
     */
    public XmlFactoryException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructs a new runtime exception that simply wraps the given factory exception.
     * @param cause the cause (which is saved for later retrieval by the Throwable.getCause() method) and is used
     * for getting the message for this XmlFactoryException
     */
    public XmlFactoryException(Throwable cause) {
        super(cause);
    }

}
