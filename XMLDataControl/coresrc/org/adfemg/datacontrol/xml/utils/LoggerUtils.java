package org.adfemg.datacontrol.xml.utils;

import java.util.HashMap;
import java.util.LinkedHashMap;

import oracle.adf.share.logging.ADFLogger;


/**
 * Utility methods for working with ADFLogger.
 */
public final class LoggerUtils {

    /**
     * Private constructor to prevent instances being created.
     */
    private LoggerUtils() {
    }

    /**
     * Invoke ADFLogger.begin without the need to first construct a Map or HashMap with the context.
     * @param logger ADFLogger instance to invoke
     * @param actionName name of the action to begin. Be sure to also invoke ADFLogger.end from a finally clause
     * to ensure the timer is always stopped.
     * @param context even number of String arguments that are used to construct the logging context where each two
     * subsequent Strings form a key-value pair for that logging context map. If non-string objects are passed they are
     * converted to strings using their toString() while null will simply remain null.
     * By using varargs it is much cleaner to
     * invoke this version than constructing a map yourself before invoking ADFLogger.begin
     * @throws IllegalArgumentException when context varargs are not an even number (0 is fine)
     * @see ADFLogger#begin
     * @see #end
     */
    @SuppressWarnings("deprecation") // need to call deprecated version with HashMap arg because that's only one in 11g
    public static void begin(ADFLogger logger, String actionName, Object... context) {
        if (context.length % 2 == 1) {
            throw new IllegalArgumentException("context should be even number of parameters (key1, value1, key2, value2, ...)");
        }
        // build Map with context (LinkedHashMap so we keep the same order)
        HashMap<String, String> contextMap = new LinkedHashMap<String, String>(context.length / 2);
        for (int i = 0; i < context.length; i += 2) {
            Object key = context[i];
            Object value = context[i + 1];
            contextMap.put(key == null ? null : key.toString(), value == null ? null : value.toString());
        }
        logger.begin(actionName, contextMap);
    }

    /**
     * Invoke ADFLogger.end on the given logger. This should always be called from a finally clause after calling
     * the LoggerUtils.begin method to ensure the timer that was started by invoking begin is always closed.
     * @param logger ADFLogger instance to invoke
     * @param actionName name of the action to end. This should be exactly the same as was used for LoggerUtils.begin
     * @see #begin
     */
    public static void end(ADFLogger logger, String actionName) {
        logger.end(actionName);
    }

    /**
     * Convenience method for invoking ADFLogger.severe with varargs.
     * @param logger ADFLogger instance to invoke
     * @param message message to log
     * @param args variable number of arguments that should have been referenced in the message with {0}, {1}, etc
     * @see ADFLogger#severe
     */
    public static void severe(ADFLogger logger, String message, Object... args) {
        logger.severe(message, args);
    }

    /**
     * Convenience method for invoking ADFLogger.warning with varargs.
     * @param logger ADFLogger instance to invoke
     * @param message message to log
     * @param args variable number of arguments that should have been referenced in the message with {0}, {1}, etc
     * @see ADFLogger#warning
     */
    public static void warning(ADFLogger logger, String message, Object... args) {
        logger.warning(message, args);
    }

    /**
     * Convenience method for invoking ADFLogger.info with varargs.
     * @param logger ADFLogger instance to invoke
     * @param message message to log
     * @param args variable number of arguments that should have been referenced in the message with {0}, {1}, etc
     * @see ADFLogger#info
     */
    public static void info(ADFLogger logger, String message, Object... args) {
        logger.info(message, args);
    }

    /**
     * Convenience method for invoking ADFLogger.fine with varargs.
     * @param logger ADFLogger instance to invoke
     * @param message message to log
     * @param args variable number of arguments that should have been referenced in the message with {0}, {1}, etc
     * @see ADFLogger#fine
     */
    public static void fine(ADFLogger logger, String message, Object... args) {
        logger.fine(message, args);
    }

    /**
     * Convenience method for invoking ADFLogger.finer with varargs.
     * @param logger ADFLogger instance to invoke
     * @param message message to log
     * @param args variable number of arguments that should have been referenced in the message with {0}, {1}, etc
     * @see ADFLogger#finest
     */
    public static void finer(ADFLogger logger, String message, Object... args) {
        logger.finer(message, args);
    }

    /**
     * Convenience method for invoking ADFLogger.finest with varargs.
     * @param logger ADFLogger instance to invoke
     * @param message message to log
     * @param args variable number of arguments that should have been referenced in the message with {0}, {1}, etc
     * @see ADFLogger#finest
     */
    public static void finest(ADFLogger logger, String message, Object... args) {
        logger.finest(message, args);
    }

}
