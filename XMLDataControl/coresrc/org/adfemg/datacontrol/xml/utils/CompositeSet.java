package org.adfemg.datacontrol.xml.utils;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Set;

public class CompositeSet<E> extends AbstractSet<E> {

    private Set<E>[] sets;

    public CompositeSet(Set<E>... sets) {
        this.sets = sets;
    }

    @Override
    public Iterator<E> iterator() {
        Iterator<E>[] iterators = new Iterator[sets.length];
        for (int i = 0; i < sets.length; i++) {
            iterators[i] = sets[i].iterator();
        }
        return new CompositeIterator<E>(iterators);
    }

    @Override
    public int size() {
        int result = 0;
        for (Set<E> set : sets) {
            result += set.size();
        }
        return result;
    }
}
