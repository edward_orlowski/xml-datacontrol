package org.adfemg.datacontrol.xml.utils;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import org.xml.sax.SAXException;

/**
 * Utility methods for getting several JAX standard classes that require expensive factories to create.
 * Instead of each client having to create its own factory it is much easier to centralize this so we can
 * optimize for factory usage in this utility class.
 */
public final class XmlFactory {

    // expensive factories
    private static final DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
    private static final TransformerFactory transformerFactory = TransformerFactory.newInstance();
    private static final SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
    private static final XPathFactory xpathFactory = XPathFactory.newInstance();

    // static initializer to further configure the factories
    static {
        docBuilderFactory.setNamespaceAware(true);
    }

    /**
     * Private constructor to prevent instances being created.
     */
    private XmlFactory() {
    }

    /**
     * Create a new DocumentBuilder for parsing and constructing DOM documents. Although this method itself
     * is thread safe due to synchronization, the returned DocumentBuilder might not be. So either
     * create a new one for each usage or be sure to use proper synchronization.
     * @return namespace aware, non validating DocumentBuilder that is not guaranteed to be thread safe
     * @see DocumentBuilder
     */
    public static DocumentBuilder newDocumentBuilder() {
        // returns namespaceAware, non-validating DocumentBuilder
        synchronized (docBuilderFactory) {
            try {
                return docBuilderFactory.newDocumentBuilder();
            } catch (ParserConfigurationException e) {
                // if a DocumentBuilder cannot be created which satisfies the configuration requested.
                // would be very strange as we have no specific configuration, so no need for checked exception
                throw new XmlFactoryException(e);
            }
        }
    }

    /**
     * Create a new, blank DOM document.
     * @return new empty DOM document.
     * @see DomUtils
     * @see #newDocumentElement
     */
    public static Document newDocument() {
        return newDocumentBuilder().newDocument();
    }

    /**
     * Create a new document with a single root document node.
     * @param namespaceURI the namespace URI of the document element to be created
     * @param qualifiedName the name of the document element to be created which might contain a namespace prefix
     * like pfx:elemName. Without a prefix the element will get a local default namespace declaration for the
     * given namespace.
     * @return document root element
     * @see DomUtils
     */
    public static Element newDocumentElement(final String namespaceURI, final String qualifiedName) {
        Document doc = newDocument();
        Element retval = doc.createElementNS(namespaceURI, qualifiedName);
        doc.appendChild(retval);
        return retval;
    }

    /**
     * Create a new copy-only, non-indenting Transformer for processing XML from a variety of sources and write the
     * unaltered output to a variaty of sinks. Although this method itself is thread safe due to synchronization, the
     * returned Transformer is not. So create a new Transformer for each usage or use proper synchronization to prevent
     * concurrent use. Once a Transformer is finished it can be used multiple times.
     * @return new Transformer instance that only copies the source to the sink without actual XSL transformation
     * @see #newIndentingTransformer
     */
    public static Transformer newNonIndentingTransformer() {
        synchronized (transformerFactory) {
            try {
                Transformer retval = transformerFactory.newTransformer();
                retval.setOutputProperty(OutputKeys.INDENT, "no");
                return retval;
            } catch (TransformerConfigurationException e) {
                // When it is not possible to create a Transformer instance
                // would be very strange as we have no specific configuration, so no need for checked exception
                throw new XmlFactoryException(e);
            }
        }
    }

    /**
     * Create a new copy-only indenting Transformer for processing XML from a variety of sources and write the unaltered
     * output to a variaty of sinks. Although this method itself is thread safe due to synchronization, the returned
     * Transformer is not. So create a new Transformer for each usage or use proper synchronization to prevent
     * concurrent use. Once a Transformer is finished it can be used multiple times.
     * @return new Transformer instance that only copies the source to the sink without actual XSL transformation
     * @see #newNonIndentingTransformer
     */
    @SuppressWarnings("oracle.jdeveloper.java.insufficient-catch-block")
    public static Transformer newIndentingTransformer() {
        Transformer retval = newNonIndentingTransformer();
        retval.setOutputProperty(OutputKeys.INDENT, "yes");
        try {
            retval.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
        } catch (IllegalArgumentException e) {
            // apparantly not using Apache xerces or default JDK7 XSLT, just ignore
        }
        return retval;
    }

    /**
     * Create a new XSL Transformer for processing XML from a variety of sources and write the
     * output to a variety of sinks. Although this method itself is thread safe due to synchronization, the returned
     * Transformer is not. So create a new Transformer for each usage or use proper synchronization to prevent
     * concurrent use. Once a Transformer is finished it can be used multiple times.
     * @param xslt Source of XSLT document used to create Transformer. Examples of XML Sources include DOMSource,
     * SAXSource, and StreamSource
     * @return new XSL Transformer instance
     */
    public static Transformer newTransformer(Source xslt) {
        // Care must be taken not to use this Transformer in multiple Threads running concurrently
        // when needed multiple times see TransformerFactory.newTemplates(Source)
        synchronized (transformerFactory) {
            try {
                return transformerFactory.newTransformer(xslt);
            } catch (TransformerConfigurationException e) {
                // When it is not possible to create a Transformer instance
                // would be very strange as we have no specific configuration, so no need for checked exception
                throw new XmlFactoryException(e);
            }
        }
    }

    /**
     * Create a new XSL Templates object for processing XML from a variety of sources and write the
     * output to a variety of sinks. The returned Templates is a compiled representation of the given XSLT source.
     * This Templates object may then be used concurrently across multiple threads. Creating a Templates object allows
     * the TransformerFactory to do the detailed performance optimization of transformation instructions once, without
     * penalizing runtime transformation.
     * @param xslt Source of XSLT document used to create Templates. Examples of XML Sources include DOMSource,
     * SAXSource, and StreamSource
     * @return new XSL Templates instance capable of being used multiple times for transformation purposes, never
     * {@code null}
     */
    public static Templates newTemplates(Source xslt) {
        synchronized (transformerFactory) {
            try {
                return transformerFactory.newTemplates(xslt);
            } catch (TransformerConfigurationException e) {
                // When it is not possible to create a Transformer instance
                // would be very strange as we have no specific configuration, so no need for checked exception
                throw new XmlFactoryException(e);
            }
        }
    }

    /**
     * Parse a XML Schema document into a javax.xml.validation.Schema which can be used for schema validation.
     * A Schema object is thread safe and applications are encouraged to share it across many parsers in many threads
     * @param schema Source that represents a schema document
     * @return New Schema from parsing {@code schema}
     * @throws SAXException if a SAX error occurs during parsing
     */
    public static Schema newSchema(Source schema) throws SAXException {
        // javadoc from javax.xml.validation.Schema: A Schema object is thread safe and applications are encouraged
        // to share it across many parsers in many threads
        synchronized (schemaFactory) {
            return schemaFactory.newSchema(schema);
        }
    }

    /**
     * Return a new default W3C DOM XPath.
     * @param namespaces Namespace context to use for resolving namespace prefixes like ns0: in the XPath. Can
     * be {@code null} to not set a namespace context.
     * @return new XPath instance
     */
    public static XPath newXPath(final NamespaceContext namespaces) {
        XPath retval;
        synchronized (xpathFactory) {
            retval = xpathFactory.newXPath();
        }
        if (namespaces != null) {
            retval.setNamespaceContext(namespaces);
        }
        return retval;
    }

}
