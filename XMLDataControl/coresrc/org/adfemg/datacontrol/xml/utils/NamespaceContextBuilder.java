package org.adfemg.datacontrol.xml.utils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.xml.namespace.NamespaceContext;


/**
 * NamespaceContext that automatically assigns a unique prefix to each namespace URI it encounters.
 * Add namespaces to this context by repeatedly invoking getPrefix if this builder can assign its own
 * unique prefixes to each namespace it encounters or repeatadly invoke addPrefix in a true builder
 * pattern as these can be chained together. When done, invoke build to get a true bi-directional
 * NamespaceContext.
 */
public class NamespaceContextBuilder implements NamespaceContext {

    private int index = 0; // used for prefix counting; ns0, ns1, ns2, ...
    private final Map<String, String> ns2prefix = new HashMap<String, String>(); // key=namespaceURI, value=prefix

    private static final NamespaceContext DFLT_NAMESPACES =
        new NamespaceContextImpl(Collections.<String, String>emptyMap());

    /**
     * Default no-arg constructor.
     */
    public NamespaceContextBuilder() {
    }

    /**
     * Gets the prefix (like ns0, ns1, ...) for the given namespace URI. When the given namespace URI
     * was previously encountered by this NamespaceContext it will return the same prefix. If the
     * namespace URI hasn't been encountered before a new unique prefix will be assigned to it for
     * future reference.
     * @param namespaceURI namespace URI to get the prefix for
     * @return unique prefix corresponding to the given namespace URI that was assigned by this
     * NamespaceContextBuilder or "xml" or "xmlns" for the two default XML namespaces
     * "http://www.w3.org/XML/1998/namespace" and "http://www.w3.org/2000/xmlns/"
     * @throws IllegalArgumentException if given namespace URI is null
     */
    @Override
    public String getPrefix(final String namespaceURI) {
        if (namespaceURI == null) {
            throw new IllegalArgumentException("namespace URI cannot be null");
        }
        final String dfltPrefix = DFLT_NAMESPACES.getPrefix(namespaceURI);
        if (dfltPrefix != null) {
            return dfltPrefix;
        }
        String retval = ns2prefix.get(namespaceURI); // lookup previously mapped namespace
        if (retval == null) {
            retval = "ns" + (index++);
            add(namespaceURI, retval);
        }
        return retval;
    }

    /**
     * Add a namespace/prefix mapping to this builder and return the same instance of this builder so multiple
     * add() invokes can be chained together.
     * <p>For example:
     * {@code new NamespaceContextBuilder().add('example.com','ex').add('something.org','s').build()}
     * @param namespaceURI namespace URI to add to this builder
     * @param prefix prefix the given namespace URI should map to
     * @return this same builder for chaining multiple add() invokes before invoking build()
     * @throws IllegalArgumentException if given namespace URI or prefix is null, or if given namespace URI or prefix
     * is one of the default namespaces that cannot be re-mapped (see NamespaceContextImpl.DEFAULT_NAMESPACES) or
     * when the same namespace or prefix is being added multiple times.
     */
    public NamespaceContextBuilder add(final String namespaceURI, final String prefix) {
        if (namespaceURI == null ||  prefix == null ) {
            throw new IllegalArgumentException("namespace URI and prefix cannot be null");
        }
        if (DFLT_NAMESPACES.getPrefix(namespaceURI) != null || DFLT_NAMESPACES.getNamespaceURI(prefix) != null) {
            throw new IllegalArgumentException("cannot override mapping for any of the default namespaces");
        }
        if (ns2prefix.containsValue(prefix)) {
            throw new IllegalArgumentException("prefix " + prefix + " already mapped");
        }
        if (ns2prefix.containsKey(namespaceURI)) {
            throw new IllegalArgumentException("namespace " + namespaceURI + " already mapped");
        }
        ns2prefix.put(namespaceURI, prefix);
        return this;
    }

    /**
     * Creates a fully functional NamespaceContext that can lookup namespace URIs by
     * prefix and the other way around.
     * @return Serializable NamespaceContext
     */
    public NamespaceContext build() {
        // reverse key and value in maps
        final Map<String, String> prefix2ns = new HashMap<String, String>(ns2prefix.size());
        for (final Map.Entry<String, String> entry : ns2prefix.entrySet()) { // key=namespaceURI, value=prefix
            prefix2ns.put(entry.getValue(), entry.getKey());
        }
        return new NamespaceContextImpl(prefix2ns);
    }

    /**
     * This NamespaceContextBuilder is only intended to discover new namespace URIs by subsequent
     * calls to getPrefix and not for lookup of namespaces by prefixes so this will always throw
     * an UnsupportedOperationException.
     * @param prefix prefix
     * @return nothing as it always throws an exception
     * @throws UnsupportedOperationException
     * @see #build
     */
    @Override
    public String getNamespaceURI(final String prefix) {
        throw new UnsupportedOperationException(NamespaceContextBuilder.class.getSimpleName() +
                                                " only supports getPrefix. Invoke createContext() to get a fully operational read-only NamespaceContext");
    }

    /**
     * This NamespaceContextBuilder is only intended to discover new namespace URIs by subsequent
     * calls to getPrefix and not for lookup of namespaces by prefixes so this will always throw
     * an UnsupportedOperationException.
     * @param namespaceURI namespace URI
     * @return nothing as it always throws an exception
     * @throws UnsupportedOperationException
     * @see #build
     */
    @Override
    public Iterator getPrefixes(final String namespaceURI) {
        throw new UnsupportedOperationException(NamespaceContextBuilder.class.getSimpleName() +
                                                " only supports getPrefix. Invoke createContext() to get a fully operational read-only NamespaceContext");
    }

}
