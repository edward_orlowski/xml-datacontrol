package org.adfemg.datacontrol.xml.ha;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;

import java.util.Collections;
import java.util.Map;

import oracle.adf.share.logging.ADFLogger;

import org.w3c.dom.Element;

/**
 * Keeps the parameters and XML element result of a datacontrol operation as Serializable
 * object so this can be used in cluster replication.
 * @see SnapshotManagerImpl
 */
public class OperationState implements Serializable {

    @SuppressWarnings("compatibility:3845922042609457543")
    private static final long serialVersionUID = 3L;

    // serializable state
    SerializableParameters paramValues; // package private for testing
    private SerializableNode<Element> element;

    // transient state to mark failover has occured
    private transient boolean failover;

    private static final ADFLogger logger = ADFLogger.createADFLogger(OperationState.class);

    /**
     * Default constructor.
     * @param paramValues parameter names and values from DataRequest that was used to retrieve this result so
     *                    we can later determine if this cached result should be used as this is only allowed if
     *                    the new DataRequest has the same parameter values.
     * @param result XML element that was returned by the datacontrol operation
     */
    public OperationState(final Map<String, Object> paramValues, final Element result) {
        this.paramValues =
            new SerializableParameters(paramValues != null ? paramValues : Collections.<String, Object>emptyMap());
        this.element = new SerializableNode<Element>(result);
    }

    /**
     * Determines if a failover has occured which means this object has been serialized and deserialized.
     * @return {@code true} if this object has been serialized and deserialized, otherwise {@code false}
     */
    public boolean isFailover() {
        return failover;
    }

    /**
     * Compares the parameter values in this OperationState with the given parameter values.
     * @param paramValues other set of parameter values
     * @return {@code true} if all keys and values in this parameter values and the given
     * parameter values are the same which means the XML element in this OperationState can be used
     * instead of invoking the datacontrol operation with the given parameters.
     * This includes support for org.w3c.dom.Node and org.w3c.dom.NodeList
     * where we can't use the plain equals but will actually compare the XML with isEqualNode
     * @see SerializableParameters#equals
     */
    public boolean isSameParamValues(final Map<String, Object> paramValues) {
        if (paramValues == null) {
            throw new IllegalArgumentException("cannot compare with null, only with map");
        }
        // this.paramValues is SerializableParameters with special handling for Node and NodeList values
        boolean retval = (this.paramValues != null && this.paramValues.equals(paramValues));
        if (!retval && logger.isFine()) {
            // could happen after failover to other server and user changes to methodAction parameters
            // before asking for the result of that methodAction
            logger.fine("different param values {0} than cached failover result {1}, ignoring failover cached result", new Object[] {
                        paramValues, this.paramValues
            });
        }
        return retval;
    }

    /**
     * Gets the XML element from this OperationState.
     * @return XML element
     */
    public Element getElement() {
        return element.getNode();
    }

    /**
     * Return human readable version of this OperationState.
     * @return human readable representation of this
     */
    @Override
    public String toString() {
        return getClass().getSimpleName() + "[failover=" + failover + ",params=" + paramValues + ",element=" + element + "]";
    }

    /**
     * Custom deserialization so we can mark this as a failover
     * @param in
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        failover = true;
    }

}
