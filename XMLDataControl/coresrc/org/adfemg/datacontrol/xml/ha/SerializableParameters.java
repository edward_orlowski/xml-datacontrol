package org.adfemg.datacontrol.xml.ha;

import java.io.IOException;
import java.io.NotSerializableException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Helper class for {@link OperationState} to ensure all parameters in a DataRequest are either
 * Serializable or are a org.w3c.dom.Node or org.w3c.dom.NodeList for which this SerializableParameters
 * has special handling to ensure they are also serializable although their interfaces don't
 * guarantee this.
 */
public class SerializableParameters implements Map<String, Object>, Serializable {

    @SuppressWarnings("compatibility:1801032378545358314")
    private static final long serialVersionUID = 1L;

    private transient Map<String, Object> map; // params map
    private transient Map<String, Serializable> serializedState; // state for lazy deserialization

    /**
     * Constructor
     * @param map map with parameters where the values have to be serializable or instances of
     * org.w3c.dom.Node or org.w3c.dom.NodeList
     */
    public SerializableParameters(Map<String, ? extends Object> map) {
        this.map = new HashMap<String, Object>(map);
    }

    @Override
    public int size() {
        ensureDeserialization();
        return map.size();
    }

    @Override
    public boolean isEmpty() {
        ensureDeserialization();
        return map.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        ensureDeserialization();
        return map.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        ensureDeserialization();
        return map.containsValue(value);
    }

    @Override
    public Object get(Object key) {
        ensureDeserialization();
        return map.get(key);
    }

    @Override
    public Object put(String key, Object value) {
        throw new UnsupportedOperationException(SerializableParameters.class.getSimpleName() + ".put");
    }

    @Override
    public Object remove(Object object) {
        throw new UnsupportedOperationException(SerializableParameters.class.getSimpleName() + ".remove");
    }

    @Override
    public void putAll(Map<? extends String, ? extends Object> map) {
        throw new UnsupportedOperationException(SerializableParameters.class.getSimpleName() + ".putAll");
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException(SerializableParameters.class.getSimpleName() + ".clear");
    }

    @Override
    public Set<String> keySet() {
        ensureDeserialization();
        return Collections.unmodifiableSet(map.keySet());
    }

    @Override
    public Collection<Object> values() {
        ensureDeserialization();
        return Collections.unmodifiableCollection(map.values());
    }

    @Override
    public Set<Map.Entry<String, Object>> entrySet() {
        ensureDeserialization();
        return Collections.unmodifiableSet(map.entrySet());
    }

    /**
     * Returns true if the given object is a Map with the same keys as this SerializableParameters
     * where each key points to an object which is equals to the object at the same key in this
     * SerializableParameters. Special handling exist for org.w3.dom.Node objects that use
     * Node.isEqualNode to compare the two. Similar special handling exists for comparing
     * org.w3c.dom.NodeList objects that have to contain the same set of Nodes in the same order
     * which again uses Node.isEqualNode to compare the two.
     * @param other Object to compare to which typically should be a Map
     * @return {@code true} when all requirements as stated in the description are true or otherwise
     * {@code false}
     * @see Node#isEqualNode
     */
    @Override
    public boolean equals(Object other) {
        ensureDeserialization();
        if (!(other instanceof Map)) {
            return false;
        }
        Map otherMap = (Map) other;
        if (size() != otherMap.size()) {
            return false;
        }
        for (Map.Entry<String, Object> entry : entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            if (!otherMap.containsKey(key)) {
                return false;
            }
            Object otherVal = otherMap.get(key);
            if (!equalsValue(value, otherVal)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Compare a set of values which is used by {@link #equals} to compare individual values in the map
     * @param thisValue
     * @param otherValue
     * @return {@code true} if both objects are equal by using their equals() method except for
     * org.w3c.dom.Node objects that use {@link Node#isEqualNode} or org.w3c.dom.NodeList objects that
     * compare each containing Node using isEqualNode
     */
    private boolean equalsValue(Object thisValue, Object otherValue) {
        if (thisValue instanceof Node) {
            return (otherValue instanceof Node && ((Node) thisValue).isEqualNode((Node) otherValue));
        } else if (thisValue instanceof NodeList) {
            return otherValue instanceof NodeList &&
                   SerializableNodeList.equalNodeList((NodeList) thisValue, (NodeList) otherValue);
        } else if (thisValue == null) {
            return otherValue == null;
        } else {
            return thisValue.equals(otherValue);
        }
    }

    // custom serialization
    private void writeObject(ObjectOutputStream out) throws IOException {
        if (serializedState != null) {
            // we still have state from previous deserialization (ensureDeserialization wasn't invoked)
            out.writeObject(serializedState);
            return;
        }
        Map<String, Serializable> state = new HashMap<String, Serializable>(map.size());
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            state.put(entry.getKey(), serializeParameter(entry.getValue()));
        }
        out.writeObject(state);
    }

    // custom deserialization to delay actual deserialization until the time it is actually needed and not as soon
    // as the object is replicated to other nodes in the cluster. Waiting for deserialization until the actual failover
    // has occured is much more efficient
    @SuppressWarnings("unchecked")
    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        serializedState = (Map<String, Serializable>) in.readObject();
    }

    // process any state that was deserialized. Should be invoked by each method that requires object state to ensure
    // full dserialization has occured as readObject delays this
    private void ensureDeserialization() {
        if (serializedState == null) {
            return;
        }
        // rebuild map from serializedState
        Map<String, Serializable> restore = serializedState;
        serializedState = null; // reset ASAP so we try this only once, even if things fail
        Map<String, Object> newmap = new HashMap<String, Object>(restore.size());
        for (Map.Entry<String, Serializable> entry : restore.entrySet()) {
            newmap.put(entry.getKey(), deserializeParameter(entry.getValue()));
        }
        map = newmap; // replace map in one go to prevent half restored map if something fails
    }

    // used as part of custom serialization to serialize individual objects in the map
    private Serializable serializeParameter(Object parameter) throws NotSerializableException {
        if (parameter == null) {
            return null;
        } else if (parameter instanceof Node) {
            return new SerializableNode<Node>((Node) parameter);
        } else if (parameter instanceof SerializableNode) {
            // if we would allow this we cannot distinguish it from a Node we've wrapped ourselves
            throw new IllegalArgumentException("Can not (yet) contain SerializableNode");
        } else if (parameter instanceof NodeList) {
            return new SerializableParameters.SerializableNodeList((NodeList) parameter);
        } else if (parameter instanceof SerializableParameters.SerializableNodeList) {
            // if we would allow this we cannot distinguish it from a NodeList we've wrapped ourselves
            throw new IllegalArgumentException("Can not (yet) contain SerializableNodeList");
        } else if (parameter instanceof Serializable) {
            return (Serializable) parameter;
        } else {
            // all other values should simply be Serializable
            throw new NotSerializableException(parameter.getClass().getName());
        }
    }

    // used as part of cistom deserialization, so should be the reverse of serializeParameter
    private Object deserializeParameter(Serializable serialized) {
        if (serialized instanceof SerializableNode) {
            return ((SerializableNode) serialized).getNode();
        } else if (serialized instanceof SerializableParameters.SerializableNodeList) {
            return ((SerializableParameters.SerializableNodeList) serialized).getNodeList();
        } else {
            return serialized;
        }
    }

    @Override
    public String toString() {
        StringBuilder retval = new StringBuilder(this.getClass().getSimpleName()).append("[");
        if (serializedState != null) {
            retval.append("serialized=").append(serializedState);
        } else {
            retval.append(map);
        }
        retval.append("]");
        return retval.toString();
    }

    /**
     * Helper class to wrap a org.w3c.dom.NodeList and make it Serializable which means a
     * (de)serialized clone will contain the same number of Nodes in the same order where
     * each Node will return {@code true} when invoking isEqualNode with its original
     * source Node.
     * Keep in mind that the source NodeList might have been a live NodeList that can change
     * when the underlying document changes. This behavior is lost after serialization as
     * we merely serialize the set of Nodes in the list at serialization, not the NodeList
     * itself.
     */
    private static class SerializableNodeList implements Serializable {
        @SuppressWarnings("compatibility:5067137899416346540")
        private static final long serialVersionUID = 2L;

        private final ArrayList<SerializableNode<Node>> serializableList;
        private transient NodeList nodelist;

        public SerializableNodeList(NodeList nodelist) {
            this.nodelist = nodelist; // simplest to return when asked for the nodelist
            // build serializable list that will survive serialization as it is non-transient
            serializableList = new ArrayList<SerializableNode<Node>>(nodelist.getLength());
            for (int i = 0, n = nodelist.getLength(); i < n; i++) {
                serializableList.add(new SerializableNode<Node>(nodelist.item(i)));
            }
        }

        public NodeList getNodeList() {
            if (nodelist == null && serializableList != null) {
                // rebuild transient nodelist lost after serialization
                final List<Node> nodes = new ArrayList<Node>(serializableList.size());
                for (SerializableNode<Node> sn : serializableList) {
                    nodes.add(sn.getNode());
                }
                nodelist = new NodeList() {
                    @Override
                    public int getLength() {
                        return nodes.size();
                    }

                    @Override
                    public Node item(int i) {
                        return nodes.get(i);
                    }

                    @Override
                    public boolean equals(Object other) {
                        return other instanceof NodeList && SerializableNodeList.equalNodeList(this, (NodeList) other);
                    }
                };
            }
            return nodelist;
        }

        @Override
        public String toString() {
            return getClass().getSimpleName() + "[" +
                   (nodelist != null ? nodelist : ("serialized=" + serializableList)) + "]";
        }

        private static boolean equalNodeList(NodeList list1, NodeList list2) {
            if (list1.getLength() != list2.getLength()) {
                return false;
            }
            for (int i = 0, n = list1.getLength(); i < n; i++) {
                if (!list1.item(i).isEqualNode(list2.item(i))) {
                    return false;
                }
            }
            return true;
        }
    }

}
