package org.adfemg.datacontrol.xml.ha;

import java.io.Serializable;

import oracle.adf.share.logging.ADFLogger;

import org.adfemg.datacontrol.xml.provider.data.DataProvider;
import org.adfemg.datacontrol.xml.provider.data.DataRequest;
import org.adfemg.datacontrol.xml.provider.filter.DataFilter;

import org.w3c.dom.Element;

/**
 * DataFilter that can create and restore a snapshot of the parameters that went into the wrapped
 * DataProvider and the XML element it returned. Can be used in a replicating HA cluster to retain
 * state across the cluster and returned the previous XML element on a failover cluster node.
 * <p>
 * Be aware that this replicates the result from each datacontrol operation to the failover node
 * in the cluster, but it does not replicate row currency so any existing iterator will reset to
 * the first row. This is because the datacontrol itself is not responsible for the iterators, but
 * the ADF Binding layer. ADF uses a Business Components Application Module (and associated ViewObjects)
 * to wrap a datacontrol and offer iterator functionality. In version 12.1.3 you can enable
 * Business Components with a combination of a number of system properties: jbo.passivationstore=file,
 * jbo.tmpdir=/some/shared/filesystem, jbo.dofailover=true and oracle.adf.model.bean.supports-passivation=true.
 * You can also use database based passivation as with normal ADF BC. Please note that the
 * oracle.adf.model.bean.supports-passivation property is an undocumented one and there might be
 * issues and Oracle might not fully support this. This feature is not available in 11.1.1.7.
 * <p>
 * DataControlDefinitionNode ensures that whatever DataFilters and DataProviders are specified in
 * the DataControls.dcx, they are always wrapped with this SnapshotManagerImpl as the root DataFilter
 * to ensure HA cluster support.
 * @see org.adfemg.datacontrol.xml.DataControlDefinitionNode#getProviderInstance
 */
public class SnapshotManagerImpl extends DataFilter implements SnapshotManager {

    private static final ADFLogger logger = ADFLogger.createADFLogger(SnapshotManagerImpl.class);
    private OperationState state; // Serializable with all state needed for caching result

    /**
     * Constructor
     * @param source nested DataProvider
     */
    public SnapshotManagerImpl(DataProvider source) {
        super(source);
    }

    @Override // by SnapshotManager interface
    public Serializable createSnapshot() {
        return state;
    }

    @Override // by SnapshotManager interface
    public void restoreSnapshot(Serializable handle) {
        if (handle == null) {
            return;
        }
        state = (OperationState) handle;
    }

    @Override // by SnapshotManager interface
    public void removeSnapshot(Serializable handle) {
        state = null;
    }

    /**
     * Get XML element from nested DataProvider unless a cluster failover has just occured and this is
     * the first time getRootElement is invoked as well as all parameters in the DataRequest are the same
     * as when the snapshot was taken on the origin server in which case the XML element that was returned
     * at the origin server in the previous call is returned this time as well.
     * <p>It is important to not re-fetch the XML form the nested DataProvider after a failover as the XML
     * element might have user changes or user data (transient attributes) that would be lose if we re-fetch
     * from the DataProvider.
     * @param request DataRequest
     * @return XML element from the nested DataProvider unless a failover has occured in which case the XML
     * element from the origin server is returned with all its changes
     */
    @Override
    public Element getRootElement(DataRequest request) {
        // detect failover situation and respond with failed over data
        if (state != null && state.isFailover()) {
            // failover has occured and we have saved state.
            logger.warning("failover detected for {0}", request.getStructureDefinition().getFullName());
            if (state.isSameParamValues(request.getDynamicParamValues())) {
                logger.fine("returning cached value from failover state {0}", state);
                return state.getElement();
            }
            // cached OperationState is stale. After failover the DC is invoked with different parameter values
            state = null;
            logger.fine("different param values so retrieving fresh data");
        }
        // TODO: detect that nested DataProvider does not manipulate the content of the DynamicParamValues map
        // call nested DataProvider to get xml element
        Element retval = getSourceElement(request);
        // remember input values and result in OperationState that will be replicated across HA cluster
        state = new OperationState(request.getDynamicParamValues(), retval);
        return retval;
    }

}
