/**
 * High Availability (HA) Cluster support to replicate datacontrol state between cluster members.
 * SnapshotManager interface (and its implementing SnapshotManagerImpl class) is a special DataFilter that is
 * always injected by DataControlDefinitionNode as the root DataFilter that will wrap any DataFilter or
 * DataProvider that is specified in the DataControls.dcx. This ensures each data control uses this
 * SnapshoManagerImpl to provide cluster failover support. See SnapshotManagerImpl for more details.
 */
package org.adfemg.datacontrol.xml.ha;

