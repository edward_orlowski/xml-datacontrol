package org.adfemg.datacontrol.xml.java.dt;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import oracle.javatools.parser.java.v2.model.JavaAnnotation;
import oracle.javatools.parser.java.v2.model.JavaClass;
import oracle.javatools.parser.java.v2.model.JavaMethod;
import oracle.javatools.parser.java.v2.model.JavaType;
import oracle.javatools.parser.java.v2.model.JavaVariable;

import org.adfemg.datacontrol.xml.java.JAnnotation;
import org.adfemg.datacontrol.xml.java.JClass;
import org.adfemg.datacontrol.xml.java.JMethod;

/**
 * Implementation of JMethod that wraps the JDeveloper design time API oracle.javatools.parser.java.v2.model.JavaMethod.
 */
public class DtMethod implements JMethod {

    private final JavaMethod method;

    /**
     * Constructor.
     * @param method Java method being represented.
     */
    public DtMethod(final JavaMethod method) {
        this.method = method;
    }

    /**
     * @inheritDoc
     */
    @Override
    @SuppressWarnings({ "oracle.jdeveloper.java.description-is-empty", "oracle.jdeveloper.java.tag-is-misplaced" })
    public String getName() {
        return method.getName();
    }

    /**
     * @inheritDoc
     */
    @Override
    @SuppressWarnings({ "oracle.jdeveloper.java.description-is-empty", "oracle.jdeveloper.java.tag-is-misplaced" })
    public JClass getReturnType() {
        JavaType javaType = method.getReturnType();
        return new DtClass((JavaClass) javaType);
    }

    /**
     * @inheritDoc
     */
    @Override
    @SuppressWarnings({ "oracle.jdeveloper.java.description-is-empty", "oracle.jdeveloper.java.tag-is-misplaced" })
    public List<JClass> getParameterTypes() {
        JavaType[] paramTypes = method.getParameterTypes();
        List<JClass> retval = new ArrayList<JClass>(paramTypes.length);
        for (JavaType p : paramTypes) {
            retval.add(new DtClass((JavaClass) p));
        }
        return retval;
    }

    /**
     * @inheritDoc
     */
    @Override
    @SuppressWarnings({ "oracle.jdeveloper.java.description-is-empty", "oracle.jdeveloper.java.tag-is-misplaced" })
    public JAnnotation getAnnotation(JClass annotationClass) {
        if (!(annotationClass instanceof DtClass)) {
            throw new IllegalArgumentException("can only compare to other design time classes");
        }
        JavaAnnotation annotation = method.getAnnotation(((DtClass) annotationClass).jcls);
        return annotation == null ? null : new DtAnnotation(annotation);
    }

    /**
     * @inheritDoc
     */
    @Override
    @SuppressWarnings({ "oracle.jdeveloper.java.description-is-empty", "oracle.jdeveloper.java.tag-is-misplaced" })
    public String toGenericString() {
        StringBuilder sb = new StringBuilder(200);
        sb.append(method.getReturnType().getQualifiedName()).append(" ");
        sb.append(method.getOwningClass().getQualifiedName()).append(".").append(method.getName()).append("(");
        @SuppressWarnings("cast")
        Collection<JavaVariable> params = (Collection<JavaVariable>) method.getParameters(); // cast needed in JDev11
        for (JavaVariable param : params) {
            if (sb.charAt(sb.length() - 1) != '(') {
                sb.append(", ");
            }
            sb.append(param.getUnresolvedType().toString()).append(" ").append(param.getName());
        }
        sb.append(")");
        return sb.toString();
    }

    /**
     * Returns the java.lang.reflect.Method wrapped in this JMethod for situations where users want to
     * break the design-time/runtime abstraction from the JClass interfaces and are willing to downcast to
     * RtMethod and get the wrapped Method, for example to invoke it through reflection.
     * @return java.lang.reflect.Method
     */
    public JavaMethod getRealMethod() {
        return method;
    }

    /**
     * compares this method to another method.
     * @param object other method to compare to
     * @return <tt>true</tt> if given object is a DtMethod representing where
     * this.getRealMethod().equals(object.getRealMethod())
     */
    @Override
    public boolean equals(Object object) {
        return object instanceof DtMethod && ((DtMethod) object).getRealMethod().equals(method);
    }

    /**
     * hashCode of the oracle.javatools.parser.java.v2.model.JavaMethod in this DtMethod.
     * @return this.getRealMethod().hashCode()
     */
    @Override
    public int hashCode() {
        return method.hashCode();
    }

}
