package org.adfemg.datacontrol.xml.java.rt;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import java.util.ArrayList;
import java.util.Collection;

import org.adfemg.datacontrol.xml.java.JAnnotation;
import org.adfemg.datacontrol.xml.java.JClass;
import org.adfemg.datacontrol.xml.java.JMethod;

/**
 * Implementation of JClass that wraps the native Java reflection API java.lang.Class.
 * @param <T> java class or interface being represented
 */
public class RtClass<T> implements JClass {

    private Class<? extends T> cls;

    /**
     * Constructor.
     * @param cls Java class being represented.
     */
    public RtClass(final Class<? extends T> cls) {
        this.cls = cls;
    }

    /**
     * @inheritDoc
     */
    @Override
    @SuppressWarnings({ "oracle.jdeveloper.java.description-is-empty", "oracle.jdeveloper.java.tag-is-misplaced" })
    public String getName() {
        return cls.getName();
    }

    /**
     * @inheritDoc
     */
    @Override
    @SuppressWarnings({ "oracle.jdeveloper.java.description-is-empty", "oracle.jdeveloper.java.tag-is-misplaced" })
    public Collection<JMethod> getMethods() {
        Method[] methods = cls.getMethods();
        Collection<JMethod> retval = new ArrayList<JMethod>(methods.length);
        for (Method m : methods) {
            retval.add(new RtMethod(m));
        }
        return retval;
    }

    /**
     * @inheritDoc
     */
    @Override
    @SuppressWarnings({ "oracle.jdeveloper.java.description-is-empty", "oracle.jdeveloper.java.tag-is-misplaced" })
    public boolean isPrimitive() {
        return cls.isPrimitive();
    }

    /**
     * @inheritDoc
     */
    @Override
    @SuppressWarnings({ "oracle.jdeveloper.java.description-is-empty", "oracle.jdeveloper.java.tag-is-misplaced" })
    public boolean isAssignableFrom(JClass subject) {
        if (!(subject instanceof RtClass)) {
            throw new IllegalArgumentException("can only compare to other runtime classes");
        }
        return cls.isAssignableFrom(((RtClass) subject).cls);
    }

    /**
     * @inheritDoc
     */
    @Override
    @SuppressWarnings({ "oracle.jdeveloper.java.description-is-empty", "oracle.jdeveloper.java.tag-is-misplaced" })
    public JAnnotation getAnnotation(JClass annotationClass) {
        for (Annotation a : cls.getAnnotations()) {
            if (a.annotationType().getName().equals(annotationClass.getName())) {
                return new RtAnnotation(a);
            }
        }
        return null;
    }

    /**
     * @inheritDoc
     */
    @Override
    @SuppressWarnings({ "oracle.jdeveloper.java.description-is-empty", "oracle.jdeveloper.java.tag-is-misplaced" })
    public Collection<JAnnotation> getAnnotations() {
        Annotation[] annotations = cls.getAnnotations();
        Collection<JAnnotation> retval = new ArrayList<JAnnotation>(annotations.length);
        for (Annotation a : annotations) {
            retval.add(new RtAnnotation(a));
        }
        return retval;
    }

    /**
     * Instantiate a new object for this class.
     * @return New instance of the java.lang.Class represented by this RtClass
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public Object newInstance() throws InstantiationException, IllegalAccessException {
        return cls.newInstance();
    }

    /**
     * Returns the java.lang.Class wrapped in this JClass for situations where users want to
     * break the design-time/runtime abstraction from the JClass interfaces and are willing to downcast to
     * RtClass and get the wrapped Class, for example to invoke it through reflection.
     * @return java.lang.Class
     */
    public Class<? extends T> getRealClass() {
        return cls;
    }

    /**
     * compares this class to another class.
     * @param object other class to compare to
     * @return <tt>true</tt> if given object is a RtClass representing where
     * this.getRealClass().equals(object.getRealClass())
     */
    @Override
    public boolean equals(Object object) {
        return object instanceof RtClass && ((RtClass) object).getRealClass().equals(cls);
    }

    /**
     * hashCode of the java.lang.Class in this RtClass.
     * @return this.getRealClass().hashCode()
     */
    @Override
    public int hashCode() {
        return cls.hashCode();
    }

    /**
     * @inheritDoc
     */
    @Override
    @SuppressWarnings({ "oracle.jdeveloper.java.tag-is-missing", "oracle.jdeveloper.java.description-is-empty",
                      "oracle.jdeveloper.java.tag-is-misplaced" })
    public String toString() {
        return RtClass.class.getSimpleName() + "[" + (cls == null ? null : cls.getName()) + "]";
    }

}
