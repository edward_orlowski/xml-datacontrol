package org.adfemg.datacontrol.xml.events;

import java.util.EventObject;

import org.adfemg.datacontrol.xml.DataControl;


/**
 * Abstract class for a DataControlEvent. Extends the EventObject class.
 *
 * All Events are constructed with a reference to the object, the "source",
 * that is logically deemed to be the object upon which the Event in question
 * initially occurred upon.
 *
 * This source needs to be an instance of the DataControl.
 *
 * @see EventObject
 * @see DataControl
 */
public abstract class DataControlEvent extends EventObject {
    @SuppressWarnings("compatibility:653943565909095863")
    private static final long serialVersionUID = 1L;

    /**
     * Public constructor for the ValidationEvent, calls the super with the
     * input argument DataControl.
     *
     * @param source the DataControl to use in the call to the super.
     */
    public DataControlEvent(final DataControl source) {
        super(source);
    }

    @Override
    public DataControl getSource() {
        return (DataControl) super.getSource();
    }
}
