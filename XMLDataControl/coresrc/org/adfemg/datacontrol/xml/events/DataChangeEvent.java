package org.adfemg.datacontrol.xml.events;

import oracle.binding.RowContext;

import org.adfemg.datacontrol.xml.DataControl;


/**
 * DataChangeEvent extending the DataControlEvent.
 *
 * @see DataControlEvent
 */
public class DataChangeEvent extends DataControlEvent {
    @SuppressWarnings("compatibility:5466472797101606558")
    private static final long serialVersionUID = 1L;

    private final RowContext rowContext;

    /**
     * Public constructor for the ValidationEvent, calls the super with the
     * input argument DataControl.
     * Sets the field rowContext.
     *
     * @param datacontrol the DataControl to use in the call to the super.
     * @param rowContext
     */
    public DataChangeEvent(final DataControl datacontrol, final RowContext rowContext) {
        super(datacontrol);
        this.rowContext = rowContext;
    }

    public RowContext getRowContext() {
        return rowContext;
    }
}
