package org.adfemg.datacontrol.xml.handler;

import org.adfemg.datacontrol.xml.data.XMLDCElement;

/**
 * Handler for the creating of an element.
 * Will fire during the creation of a new element.
 *
 * Compare this with the "create" function in ADF BC on an Entity or ViewRow.
 */
public interface CreatedHandler extends ElementHandler {
    public void invokeCreated(XMLDCElement element);
}
