package org.adfemg.datacontrol.xml.handler;

import java.util.Map;

import org.adfemg.datacontrol.xml.data.XMLDCElement;


/**
 * Handler for a operation on the datacontrol.
 */
public interface OperationHandler extends ElementHandler {
    /**
     * Implement this check to verify if we should be concerned about the this operation.
     *
     * @param name The operationName to check for.
     * @param arguments The arguments of the operation.
     *
     * @return {@code true} if the handler is handling this operation,
     *         {@code false} if the handler is not handling this operation.
     */
    boolean handlesOperation(String name, Map arguments);

    /**
     * The invoke of the operation.
     *
     * @param element The first element always needs to be an XMLDCElement.
     * @param arguments A map of all the other arguments to the operation.
     * @return The result of the fired operation.
     */
    Object invokeOperation(XMLDCElement element, Map arguments);
}
