package org.adfemg.datacontrol.xml.handler;

/**
 * A specific interface for handlers that handle elements.
 */
public interface ElementHandler extends Handler {
}
