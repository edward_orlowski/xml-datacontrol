package org.adfemg.datacontrol.xml.handler;

import org.adfemg.datacontrol.xml.events.AttrChangeEvent;

/**
 * This handler is a hook for the postPut.
 * Meaning this logic will be invoked after the actual put has been done
 * on the element.
 */
public interface PostPutHandler extends AttributeHandler {
    /**
     * Implement your logic in this method.
     *
     * @param attrChangeEvent The AttrChangeEvent that triggered the put.
     */
    public void doPostPut(AttrChangeEvent attrChangeEvent);
}
