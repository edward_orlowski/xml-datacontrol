package org.adfemg.datacontrol.xml;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.adfemg.datacontrol.xml.provider.DynamicParameter;
import org.adfemg.datacontrol.xml.provider.Provider;
import org.adfemg.datacontrol.xml.provider.ProviderImpl;
import org.adfemg.datacontrol.xml.utils.ClassUtils;
import org.adfemg.datacontrol.xml.utils.CompositeSet;

import org.w3c.dom.Node;


/**
 * A Datacontrol provider as typically represented by a {@code something-provider} tag in the DataControls.dcx
 * file under a definition node that represents a datacontrol operation.
 * <p>Extends ProviderImpl so it can easily receive, store and retrieve all fixed
 * parameters (string, list and xml). Has additional support for dynamic-parameters.
 * @see Provider
 * @see ProviderImpl
 */
public class DataControlProviderDefinition extends ProviderImpl {

    private String className;
    private Set<DynamicParameter> dynamicParams = new LinkedHashSet<DynamicParameter>();
    private List<DataControlProviderDefinition> nestedProviderDefinitions =
        Collections.synchronizedList(new ArrayList<DataControlProviderDefinition>(2));

    private Set<String> stringParameterNames = new HashSet<String>(5);
    private Set<String> listParameterNames = new HashSet<String>(2);
    private Set<String> xmlParameterNames = new HashSet<String>(2);

    /**
     * Public constructor.
     * @param className the java class name that implements this provider, for example
     * org.adfemg.datacontrol.xml.provider.data.WSDataProvider.
     */
    public DataControlProviderDefinition(final String className) {
        this.className = className;
    }

    /**
     * Returns the java class name that implements this provider.
     * @return java class name, for example org.adfemg.datacontrol.xml.provider.data.WSDataProvider
     * @see #loadClass
     */
    public String getClassName() {
        return className;
    }

    /**
     * Adds a dynamic parameter definition.
     * @param dynpar the description of the dynamic parameter which relates to a {@code dynamic-parameter} node
     * in the DataControls.dcx
     */
    public void addDynamicParam(final DynamicParameter dynpar) {
        dynamicParams.add(dynpar);
    }

    /**
     * Get a dynamic parameter definition based on its name.
     * @param name name of the dynamic parameter definition to et
     * @return DynamicParameter describing this dynamic parameter or {@code null} if none was found and this
     * none was specified in the DataControls.dcx file.
     */
    public DynamicParameter getDynamicParam(final String name) {
        for (DynamicParameter dynpar : dynamicParams) {
            if (name != null && name.equals(dynpar.getName())) {
                return dynpar;
            }
        }
        return null;
    }

    /**
     * Gets all dynamic parameters from this datacontrol provider including any nested datacontrol
     * provider.
     * @return unmodifiable set of all dynamic parameters for this provider and all of its children
     */
    public Set<DynamicParameter> getAllDynamicParams() {
        Set<DynamicParameter> result = Collections.unmodifiableSet(dynamicParams);
        for (DataControlProviderDefinition nestedProvider : getNestedProviderDefinitions()) {
            Set<DynamicParameter> nestedParams = nestedProvider.getAllDynamicParams();
            result = new CompositeSet<DynamicParameter>(result, nestedParams);
        }
        return result;
    }

    /**
     * Loads the class implementing this datacontrol provider. Does a sanity check that the loaded
     * class implements the requested interface. This can be used so a {@code customization-provider}
     * DataControlProviderDefinition is only used to load class that implement
     * org.adfemg.datacontrol.xml.provider.customization.CustomizationProvider
     * @param <T> Type the class should implement
     * @param iface Interface the implementation class should implement
     * @return Class implementing the interface.
     * @throws IllegalArgumentException when the loaded class does not exist or it does not implement
     * the requested interface
     */
    @SuppressWarnings({ "cast", "unchecked" })
    public <T extends Provider> Class<? extends T> loadClass(final Class<T> iface) {
        // TODO: would be much nicer if DataControlProviderDefinition just knows about its type
        // could be generics or simply a Class constructor argument
        if (className == null) {
            return null;
        }
        Class<?> cls = ClassUtils.loadClass(className);
        if (cls != null && !iface.isAssignableFrom(cls)) {
            throw new IllegalArgumentException(className + " does not implement interface " + iface.getName());
        }
        return (Class<? extends T>) cls;
    }

    /**
     * Register a nested DataControlProviderDefinition. For example a {@code data-provider} can have one or
     * more nested {@code data-provider}'s.
     * @param nestedProviderDefinition DataControlProviderDefintion to register as a child. The order in which
     * these are added will be kept and nested providers will be returned in that order by getNestedProviderDefinitions()
     * @see #getNestedProviderDefinitions
     */
    public synchronized void addNestedProviderDefinition(DataControlProviderDefinition nestedProviderDefinition) {
        nestedProviderDefinitions.add(nestedProviderDefinition);
    }

    /**
     * Gets the list of nested DataControlProviderDefinitions. This list is in the same order as the children
     * were added by repeatedly invoking addNestedProviderDefinition.
     * @return ordered list of nested providers, which might be empty, but never {@code null}
     */
    public List<DataControlProviderDefinition> getNestedProviderDefinitions() {
        return Collections.unmodifiableList(nestedProviderDefinitions);
    }

    @Override
    public void setStringParameter(String name, String value) {
        super.setStringParameter(name, value);
        stringParameterNames.add(name);
    }

    @Override
    public void setListParameter(String name, List<String> values) {
        super.setListParameter(name, values);
        listParameterNames.add(name);
    }

    @Override
    public void setXmlParameter(String name, Node xml) {
        super.setXmlParameter(name, xml);
        xmlParameterNames.add(name);
    }

    public Set<String> getStringParameterNames() {
        return Collections.unmodifiableSet(stringParameterNames);
    }

    public Set<String> getListParameterNames() {
        return Collections.unmodifiableSet(listParameterNames);
    }

    public Set<String> getXmlParameterNames() {
        return Collections.unmodifiableSet(xmlParameterNames);
    }

    /**
     * Build human readable representation of this provider which includes the java class name that should implement
     * this provider at runtime.
     * @return human readable representation of this class
     */
    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "[" + getClassName() + "]";
    }

}
