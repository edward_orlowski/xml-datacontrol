package org.adfemg.datacontrol.tester.customizers;

import org.adfemg.datacontrol.xml.annotation.AttrValidation;
import org.adfemg.datacontrol.xml.annotation.CalculatedAttr;
import org.adfemg.datacontrol.xml.annotation.ElementCustomization;
import org.adfemg.datacontrol.xml.annotation.ElementValidation;
import org.adfemg.datacontrol.xml.annotation.PostAttrChange;
import org.adfemg.datacontrol.xml.annotation.TransientAttr;
import org.adfemg.datacontrol.xml.data.XMLDCElement;
import org.adfemg.datacontrol.xml.events.AttrChangeEvent;

@ElementCustomization(target = "org.adfemg.datacontrol.tester.HRDataControl.getXML.DepartmentList.Department")
public class DepartmentCust {
    public DepartmentCust() {
        super();
    }

    @CalculatedAttr
    public String getNameId(XMLDCElement element) {
        return element.get("name") + " : " + element.get("id");
    }

    @TransientAttr
    public String initComment(XMLDCElement element) {
        return "Place your comments in here";
    }

    @ElementValidation(attr = "name")
    public void check(XMLDCElement department) {
        System.out.println("check");
    }

    @ElementValidation(attr = { "name", "id" })
    public void multiAttrValidation(XMLDCElement department) {
        System.out.println("multiAttrValidation");
    }

    @PostAttrChange
    public void nameChanged(AttrChangeEvent event) {
        System.out.println("name changed");
    }

    @AttrValidation
    public void validateName(AttrChangeEvent event) {
        System.out.println("validateName");
    }

}
