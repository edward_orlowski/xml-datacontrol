package org.adfemg.datacontrol.tester.customizers;

import java.math.BigDecimal;

import oracle.jbo.AttrValException;

import org.adfemg.datacontrol.xml.annotation.AttrValidation;
import org.adfemg.datacontrol.xml.annotation.ElementCustomization;
import org.adfemg.datacontrol.xml.annotation.TransientAttr;
import org.adfemg.datacontrol.xml.data.XMLDCElement;
import org.adfemg.datacontrol.xml.events.AttrChangeEvent;

@ElementCustomization(target =
                      "org.adfemg.datacontrol.tester.HumanResourcesService.getDeptEmps.DepartmentEmployeesResponse.Employee")
public class EmployeeCust {

  @TransientAttr
  public boolean initShowJobInfo(XMLDCElement employee) {
    return false;
  }

  @AttrValidation
  public void validateSalary(AttrChangeEvent event) throws AttrValException {
    BigDecimal oldVal = (BigDecimal) event.getOldValue();
    BigDecimal newVal = (BigDecimal) event.getNewValue();
    if (newVal != null && oldVal != null && newVal.compareTo(oldVal) < 0) {
      throw new AttrValException(AttrValException.TYP_ATTRIBUTE, "Salary can not be lowered", "SAL-0001",
                                 event.getElement().getDefinition().getFullName(), event.getAttribute());
    }
  }
}
